import { GraczAdminPage } from './app.po';

describe('gracz-admin App', () => {
  let page: GraczAdminPage;

  beforeEach(() => {
    page = new GraczAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
