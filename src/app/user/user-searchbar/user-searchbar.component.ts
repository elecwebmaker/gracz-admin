import { Component, OnInit } from '@angular/core';
// import { ICustomerFilter, CustomerGetterService } from "app/customer/customer-getter.service";
import { FilterBar } from "craftutility/";
import { KeywordListService } from "app/core/keyword-list.service";
import { UserGetterService, IUserListFilter } from "app/user/user-getter.service";

@Component({
    selector: 'app-user-searchbar',
    templateUrl: './user-searchbar.component.html',
    styleUrls: ['./user-searchbar.component.scss']
})
export class UserSearchbarComponent implements OnInit {
    public filter = new FilterBar<IUserListFilter>(["key", "status"]);
    constructor(private userGetter: UserGetterService, private keywordlist: KeywordListService) { }

    ngOnInit() {
        this.filter.filterChanges().subscribe((res: any) => {
            this.userGetter.filter.setLockFilter({
                key: res.key,
                job_type: res.status,
            });
            this.userGetter.resetPage.next();
        });
        this.filter.setStatusList(
            this.keywordlist.listJobType()
        );
    }

    searchChange(val) {
        this.filter.setFilter('key', val);
    }

    statusChange(val) {
        this.filter.setFilter('status', val);
    }
}
