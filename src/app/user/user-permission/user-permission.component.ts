import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  EventEmitter,
  Output,
  ViewChild,
  OnChanges
} from "@angular/core";
import { IPermission, IList } from "app/core/model/user-permission-page.model";
import { FormArray, FormControl } from "@angular/forms/";

@Component({
    selector: 'app-user-permission',
    templateUrl: './user-permission.component.html',
    styleUrls: ['./user-permission.component.scss']
})
export class UserPermissionComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() data: IPermission | undefined;
    @Input() loading: boolean;
    @Output() list: EventEmitter<Array<number>> = new EventEmitter<Array<number>>();
    private arrayPermission: Array<number> = [];
    sale_form = new FormArray([]);
    sale_admin_form = new FormArray([]);
    chk_s: boolean;
    chk_sa: boolean;
    constructor() { }

    ngOnInit() {
        if (!this.data) {
            this.data = <IPermission>{};
        }
        setTimeout(() => {
            this.sale_form.valueChanges.subscribe(() => {
                this.chkAllManage();
            });
            this.sale_admin_form.valueChanges.subscribe(() => {
                this.chkAllManage();
            });
        }, 0);
    }

    ngAfterViewInit() {
        this.chkAllManage();
    }

    ngOnChanges() {
        if (this.data) {
            if (this.data.sale && this.data.sale.length > this.sale_form.length) {
                this.data.sale.forEach(element => {
                    this.sale_form.push(new FormControl(element.isChk));
                });
            }
            if (this.data.sale_admin && this.data.sale_admin.length > this.sale_admin_form.length) {
                this.data.sale_admin.forEach(element => {
                    this.sale_admin_form.push(new FormControl(element.isChk));
                });
            }
        }
    }

    checkAll(formA: FormArray, state: boolean) {
        for (let i = 0; i < formA.length; i++) {
            formA.at(i).setValue(state);
        }
    }

    chkValue(formA: FormArray, permission: Array<IList>) {
        for (let i = 0; i < formA.length; i++) {
            if (formA.at(i).value && permission[i]) {
                this.arrayPermission.push(permission[i].id);
            }
        }
    }

    onSubmit() {
        this.arrayPermission = [];
        this.chkValue(this.sale_admin_form, this.data.sale_admin);
        this.chkValue(this.sale_form, this.data.sale);
        this.list.emit(this.arrayPermission);
    }

    chkAllManage() {
        if (this.sale_form.value.every((e) => e === true)) {
            this.chk_s = true;
        } else {
            this.chk_s = false;
        }
        if (this.sale_admin_form.value.every((e) => e === true)) {
            this.chk_sa = true;
        } else {
            this.chk_sa = false;
        }
    }
}
