import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { UserListPageComponent } from "app/user/user-list-page/user-list-page.component";
import { UserAddFormContainerComponent } from "app/user/user-add-form-container/user-add-form-container.component";
import { UserAddPageComponent } from "app/user/user-add-page/user-add-page.component";
import { UserEditPageComponent } from "app/user/user-edit-page/user-edit-page.component";
import { UserArchivedListPageComponent } from "app/user/user-archived-list-page/user-archived-list-page.component";
import { UserPermissionPageComponent } from "app/user/user-permission-page/user-permission-page.component";
import { AuthorizeUserGuard } from "app/core/guards/authorize-user.guard";



const routes: Routes = [
    {
        path: 'user',
        canActivate: [CfAuthenJWTGuard, AuthorizeUserGuard],
        children:[
            {
                path:'',
                redirectTo:'list',
                pathMatch:'full'
            },
            {
                path:'list',
                component: UserListPageComponent
            },{
                path:'add',
                component: UserAddPageComponent
            },
            {
                path:'edit/:user_id',
                component: UserEditPageComponent
            }, {
                path: 'archived',
                component: UserArchivedListPageComponent
            }, {
                path: ':user_id/permission',
                component: UserPermissionPageComponent
            }
        ]
    },{
        path: '**', redirectTo: '/404'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
})
export class UserRoutingModule { }
