import { Injectable } from '@angular/core';
import { Http } from "@angular/http/";
import { UrlProviderService, LIST_PERMISSION, USER_GET, PERMISSION_GET } from "app/shared/url-provider.service";
import { IPermission, IUserDetail, IUserPermissionPageModel } from "app/core/model/user-permission-page.model";
import { Observable } from "rxjs/Rx";

@Injectable()
export class UserPermissionPageGetterService {

    constructor(
        private http: Http,
        private urlProvider: UrlProviderService
    ) { }

    listPermission(): Observable<IPermission> {
        const url = this.urlProvider.getUrl(LIST_PERMISSION);
        return this.http.post(url, {}).map((res: any) => {
            const result = res.model;
            return {
                sale: result[0].permission,
                sale_admin: result[1].permission
            }
        });
    }

    getDetail(id: number): Observable<IUserDetail> {
        const url = this.urlProvider.getUrl(USER_GET);
        return this.http.post(url + `/${id}`, {}).map((res: any) => {
            const result = res.model;
            return {
                id: result.id,
                username: result.username,
                name: result.first_name + ' ' + result.last_name,
                pic_src: result.picture_src,
                job_type: result.job_type
            };
        });
    }

    getPageDetail(id: number): Observable<IUserPermissionPageModel> {
        return Observable.forkJoin(this.getDetail(id), this.getPermissionList(id)).map((res) => {
            const tmp = {detail: null, permission: null};
            tmp.detail = res[0];
            if (res[0].job_type === 1) {
                tmp.permission = {
                    sale: res[1].sale,
                    sale_admin: undefined
                };
            } else if (res[0].job_type === 2) {
                tmp.permission = {
                    sale: res[1].sale,
                    sale_admin: res[1].sale_admin
                };
            }
            console.log(tmp);
            return tmp;
        });
    }

    getPermission(id: number): Observable<number[]> {
        const url = this.urlProvider.getUrl(PERMISSION_GET);
        return this.http.post(url, {user_id: id}).map((res: any) => {
            const result = res.permission;
            return result;
        });
    }

    getPermissionList(id: number): Observable<IPermission> {
        return Observable.forkJoin(this.listPermission(), this.getPermission(id)).map((res: any) => {
            res[0].sale.forEach(element => {
                element.isChk = res[1].includes(element.id);
            });
            res[0].sale_admin.forEach(element => {
                element.isChk = res[1].includes(element.id);
            });
            return res[0];
        });
    }

}
