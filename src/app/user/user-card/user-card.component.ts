import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { UserListModel } from "app/core/model/user-list.model";
import { ICardAction } from "app/user/user-list/user-list.component";
declare var $: any;

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnChanges {
    @Input() data: UserListModel;
    @Output() action: EventEmitter<ICardAction> = new EventEmitter<ICardAction>();
    constructor() { }

    ngOnChanges() {
        // console.log('card->', this.data);
    }

    ngAfterViewInit() {
        $('.ui.dropdown.button').dropdown({
            on: 'hover',
            action: 'nothing'
        });
    }

    setAction(item: string) {
        console.log(item, this.data.id);
        if (item === 'genPass') {
            this.action.emit({
                id: this.data.id,
                cmd: item,
                user_data: {
                    pic_src: this.data.pic.url,
                    name: this.data.name + ' ' + this.data.lastname
                }
            });
        } else {
            this.action.emit({id: this.data.id, cmd: item});
        }
    }
}
