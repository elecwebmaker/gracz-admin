import { TestBed, inject } from '@angular/core/testing';

import { UserGetterService } from './user-getter.service';

describe('UserGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserGetterService]
    });
  });

  it('should ...', inject([UserGetterService], (service: UserGetterService) => {
    expect(service).toBeTruthy();
  }));
});
