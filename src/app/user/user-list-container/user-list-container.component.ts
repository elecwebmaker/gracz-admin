import { Component, OnInit, ViewChild,OnDestroy } from '@angular/core';
import { IUserListModel, UserListModel } from "app/core/model/user-list.model";
import { UserGetterService } from "app/user/user-getter.service";
import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";
import { CfIContainerComp } from "app/shared/cf-comp-dispatcher/IcontainerComp";
import { ICardAction } from "app/user/user-list/user-list.component";
import { UserSenderService } from "app/user/user-sender.service";
import { UserGenPassComponent } from "app/user/user-gen-pass/user-gen-pass.component";
import { Router } from "@angular/router/";
import { Subscription, Subject } from 'rxjs/Rx';

@Component({
    selector: 'app-user-list-container',
    templateUrl: './user-list-container.component.html',
    styleUrls: ['./user-list-container.component.scss']
})
export class UserListContainerComponent implements OnInit , OnDestroy, CfIContainerComp<{model:UserListModel[],total:number}> {
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this.subscriptionLoading.unsubscribe();
    }
    data_dispatcher: CfDataDispatcher<{model:UserListModel[],total:number}> = new CfDataDispatcher<{model:UserListModel[],total:number}>();
    @ViewChild(UserGenPassComponent) genPassModal: UserGenPassComponent;
    subscription:Subscription;
    subscriptionLoading:Subscription;
    totaldata: number;
    resetPage:Subject<any>;
    constructor(
        private userGetter: UserGetterService,
        private userSender: UserSenderService,
        private router: Router
    ) { }
    ngOnInit() {
        this.userGetter.filter.setLockFilter({archived: 0});
        this.subscriptionLoading = this.userGetter.filter.searchObservable.subscribe(()=>{
            this.data_dispatcher.clearData();
        });
        this.subscription = this.userGetter.list().subscribe((data:any) => {
            this.data_dispatcher.setData(data.model);
            this.totaldata = data.total;
        });

        this.resetPage = this.userGetter.resetPage;
    }

    manage(action: ICardAction) {
        console.log(action);
        if (action.cmd === 'permission') {
            this.permissionUser(action.id);
        } else if (action.cmd === 'edit') {
            this.editUser(action.id);
        } else if (action.cmd === 'genPass') {
            this.generatePassword(action.id, action.user_data);
        } else if (action.cmd === 'archived') {
            this.archiveUser(action.id);
        }
    }

    archiveUser(id: number) {
        this.userSender.setArchived(id, 1).subscribe(()=>{
            this.data_dispatcher.clearData();
            this.userGetter.reload();
        });
    }

    generatePassword(id: number, user_data: {pic_src: string, name: string}){
        this.genPassModal.show(id, user_data);
    }

    permissionUser(id: number) {
        this.router.navigate([`user/${id}/permission`]);
    }

    editUser(id: number) {
        this.router.navigate([`user/edit/${id}`]);
    }

    onNavChange({size,offset}){
        this.userGetter.filter.setLockFilter({
            size,
            offset
        });
    }
}
