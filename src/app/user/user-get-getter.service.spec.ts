import { TestBed, inject } from '@angular/core/testing';

import { UserGetGetterService } from './user-get-getter.service';

describe('UserGetGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserGetGetterService]
    });
  });

  it('should ...', inject([UserGetGetterService], (service: UserGetGetterService) => {
    expect(service).toBeTruthy();
  }));
});
