import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router/";

@Component({
    selector: 'app-user-permission-page',
    templateUrl: './user-permission-page.component.html',
    styleUrls: ['./user-permission-page.component.scss']
})
export class UserPermissionPageComponent implements OnInit {
    public user_id: number;
    constructor(
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.user_id = this.activatedRoute.snapshot.params['user_id'];
    }

}
