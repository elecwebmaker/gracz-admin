import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionPageComponent } from './user-permission-page.component';

describe('UserPermissionPageComponent', () => {
  let component: UserPermissionPageComponent;
  let fixture: ComponentFixture<UserPermissionPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
