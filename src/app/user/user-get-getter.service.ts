import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { UrlProviderService, USER_GET, GEN_PASSWORD } from "app/shared/url-provider.service";
import { UserListModel } from "app/core/model/user-list.model";

@Injectable()
export class UserGetGetterService {

  constructor(private http: Http,private urlprovider: UrlProviderService) { }

  get(id:string){
    const url = this.urlprovider.getUrl(USER_GET);
    return this.http.post(url + `/${id}`,{}).map((res:any)=>{
      const response = res.model;
      return new UserListModel({
        email:response.email,
        id:response.id,
        isArchived:false,
        job_type:response.job_type,
        lastname:response.last_name,
        line_id:response.line_id,
        name:response.first_name,
        password:"",
        phone:{
          code:response.phone_code,
          number:response.phone_number 
        },
        pic:{
          url:response.picture_src,
          tmp:response.pictrue_database 
        },
        username:response.username,
        sale_mc5_code:response.sale_code
      })
    });
  }


  genPassword(){
    const url = this.urlprovider.getUrl(GEN_PASSWORD);
    return this.http.post(url,{}).map((res:any)=>{
      return res.password;
    });
  }
}
