import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArchivedListPageComponent } from './user-archived-list-page.component';

describe('UserArchivedListPageComponent', () => {
  let component: UserArchivedListPageComponent;
  let fixture: ComponentFixture<UserArchivedListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArchivedListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArchivedListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
