import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-archived-list-page',
  templateUrl: './user-archived-list-page.component.html',
  styleUrls: ['./user-archived-list-page.component.scss']
})
export class UserArchivedListPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
