import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserArchivedListContainerComponent } from './user-archived-list-container.component';

describe('UserArchivedListContainerComponent', () => {
  let component: UserArchivedListContainerComponent;
  let fixture: ComponentFixture<UserArchivedListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserArchivedListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserArchivedListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
