import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserListModel } from "app/core/model/user-list.model";
import { CfIContainerComp } from "app/shared/cf-comp-dispatcher/IcontainerComp";
import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";
import { UserGetterService } from "app/user/user-getter.service";
import { ICardAction } from "app/user/user-list/user-list.component";
import { UserSenderService } from "app/user/user-sender.service";
import { Subscription, Subject } from 'rxjs';

@Component({
    selector: 'app-user-archived-list-container',
    templateUrl: './user-archived-list-container.component.html',
    styleUrls: ['./user-archived-list-container.component.scss']
})
export class UserArchivedListContainerComponent implements OnInit,OnDestroy, CfIContainerComp<any> {
    data_dispatcher: CfDataDispatcher<any> = new CfDataDispatcher<any>();
    totaldata: number;
    subscription:Subscription;
    resetPage:Subject<any>;
    constructor(
        private userGetter: UserGetterService,
        private userSender: UserSenderService
    ) { }

    ngOnInit() {
        this.userGetter.filter.setLockFilter({archived: 1});
        this.subscription = this.userGetter.list().subscribe((data:any) => {
            this.data_dispatcher.setData(data.model);
            this.totaldata = data.total;
        });
        this.resetPage = this.userGetter.resetPage;
    }
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
    manage(action: ICardAction) {
        if (action.cmd === 'unarchived') {
            this.unArchiveUser(action.id);
        }
    }

    unArchiveUser(id: number) {
        this.userSender.setArchived(id, 0).subscribe(()=>{
            this.data_dispatcher.clearData();
            this.userGetter.reload();
        });
    }

    onNavChange({size,offset}){
        this.userGetter.filter.setLockFilter({
            size,
            offset
        });
    }
}
