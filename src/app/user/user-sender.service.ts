import { Injectable } from '@angular/core';
import { UserListModel } from "app/core/model/user-list.model";
import { Http } from "@angular/http";
import { UrlProviderService, USER_ADD, USER_EDIT, ARCHIVED_SET, PERMISSION_SET, CHK_CODE_MC5 } from "app/shared/url-provider.service";
import { NotifyService } from "craftutility";

const parseBody = function (user: UserListModel) {
    return {
        username: user.username,
        password: user.password,
        job_type: user.job_type,
        first_name: user.name,
        last_name: user.lastname,
        phone_code: user.phone.code,
        phone_number: user.phone.number,
        email: user.email,
        line_id: user.line_id,
        picture_src: user.pic.tmp,
        sale_code:user.sale_mc5_code
    }
}

@Injectable()
export class UserSenderService {

    constructor(private http: Http, private urlprovider: UrlProviderService, private notifyhandler: NotifyService) {

    }

    add(user: UserListModel) {
        console.log(user, 'user');
        const url = this.urlprovider.getUrl(USER_ADD);
        const body = parseBody(user);
        return this.http.post(url, body).map((e) => {
            this.notifyhandler.showSuccess();
        });
    }
    setArchived(u_id: number, status: number) {
        const url = this.urlprovider.getUrl(ARCHIVED_SET);
        const body = {
            user_id: u_id,
            archived_status: status
        }
        return this.http.post(url, body).map(() => {
            this.notifyhandler.showSuccess();
        });
    }
    edit(user: UserListModel) {
        const url = this.urlprovider.getUrl(USER_EDIT) + `/${user.id}`;
        const body = parseBody(user);
        return this.http.post(url, body).map(() => {
            this.notifyhandler.showSuccess();
        });
    }
    editPwd(id: number, body: {password: string}) {
        const url = this.urlprovider.getUrl(USER_EDIT) + `/${id}`;
        return this.http.post(url, body).map(() => {
            this.notifyhandler.showSuccess();
        });
    }
    setPermission(id: number, permission: Array<number>) {
        const url = this.urlprovider.getUrl(PERMISSION_SET);
        const body = {
            user_id: id,
            permission: JSON.stringify(permission)
        };
        return this.http.post(url, body).map(() => {
            this.notifyhandler.showSuccess();
        });
    }

    // chkSaleMc5(salecode){
    //     const url = this.urlprovider.getUrl(CHK_CODE_MC5);
    //     return this.http.post(url, {}).map((res:any)=>{
    //         return res.success;
    //     })
    // }

}
