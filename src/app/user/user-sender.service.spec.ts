import { TestBed, inject } from '@angular/core/testing';

import { UserSenderService } from './user-sender.service';

describe('UserSenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserSenderService]
    });
  });

  it('should ...', inject([UserSenderService], (service: UserSenderService) => {
    expect(service).toBeTruthy();
  }));
});
