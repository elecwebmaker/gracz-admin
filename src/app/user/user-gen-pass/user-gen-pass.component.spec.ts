import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGenPassComponent } from './user-gen-pass.component';

describe('UserGenPassComponent', () => {
  let component: UserGenPassComponent;
  let fixture: ComponentFixture<UserGenPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGenPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGenPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
