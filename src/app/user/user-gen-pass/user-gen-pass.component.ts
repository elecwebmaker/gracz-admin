import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SemanticModalComponent } from "ng-semantic/src/modal/modal";
import { UserGetterService } from "app/user/user-getter.service";
import { NotifyService } from "craftutility/";
import { UserSenderService } from "app/user/user-sender.service";

@Component({
    selector: 'app-user-gen-pass',
    templateUrl: './user-gen-pass.component.html',
    styleUrls: ['./user-gen-pass.component.scss']
})
export class UserGenPassComponent implements OnInit {
    @ViewChild(SemanticModalComponent) confirmModal: SemanticModalComponent;
    public u_id: number;
    public data: {pic_src: string, name: string} = {pic_src: '', name: 'firstname lastname'};
    public pwd: string;
    public loading_gen: boolean = false;
    public loading: boolean = false;
    constructor(
        private userGetter: UserGetterService,
        private userSetter: UserSenderService,
        private notify: NotifyService
    ) { }

    ngOnInit() {
        this.confirmModal.hide();
    }

    show(id: number, user_data: {pic_src: string, name: string}) {
        this.u_id = id;
        this.data = user_data;
        this.confirmModal.show();
        this.genPwd();
    }

    hide() {
        this.confirmModal.hide();
    }

    genPwd() {
        this.loading_gen = true;
        this.userGetter.genPassword().subscribe((res) => {
            this.pwd = res;
            this.loading_gen = false;
        });
    }

    confirm() {
        this.loading = true;
        this.userSetter.editPwd(this.u_id, {password: this.pwd}).subscribe(() => {
            this.loading = false;
            this.notify.showSuccess();
            this.confirmModal.hide();
        });
    }

    cancel() {
        this.confirmModal.hide();
    }
}
