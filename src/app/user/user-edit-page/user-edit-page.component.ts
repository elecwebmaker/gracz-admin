import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-user-edit-page',
  templateUrl: './user-edit-page.component.html',
  styleUrls: ['./user-edit-page.component.scss']
})
export class UserEditPageComponent implements OnInit {
  user_id: string;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.user_id = this.activatedRoute.snapshot.params['user_id'];
  }

}
