import { Component, OnInit, Input, forwardRef, OnChanges, Output, EventEmitter } from '@angular/core';
import { IUserListModel, UserListModel } from "app/core/model/user-list.model";
import { CfPresentComponent } from "craftutility";

export interface ICardAction {
    id: number;
    cmd: string;
    user_data?: {
        pic_src: string;
        name: string;
    }
}

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    providers: [
        {
            provide: CfPresentComponent,
            useExisting: forwardRef(() => UserListComponent)
        }
    ]
})
export class UserListComponent extends CfPresentComponent<UserListModel[]> implements OnChanges {
    @Input() data: UserListModel[];
    @Output() action: EventEmitter<ICardAction> = new EventEmitter<ICardAction>();
    constructor() {
        super();
    }

    ngOnChanges() {
        // console.log('user-list said ->', this.data);
    }

    setAction(action: ICardAction) {
        this.action.emit(action);
    }
}
