import { Component, OnInit, Input } from '@angular/core';
import { IUserDetail } from "app/core/model/user-permission-page.model";

@Component({
    selector: 'app-user-permission-detail',
    templateUrl: './user-permission-detail.component.html',
    styleUrls: ['./user-permission-detail.component.scss']
})
export class UserPermissionDetailComponent implements OnInit {
    @Input() data: IUserDetail | undefined;
    constructor() { }

    ngOnInit() {
        if (!this.data) {
            this.data = <IUserDetail>{};
        }
    }

}
