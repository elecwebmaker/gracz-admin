import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionDetailComponent } from './user-permission-detail.component';

describe('UserPermissionDetailComponent', () => {
  let component: UserPermissionDetailComponent;
  let fixture: ComponentFixture<UserPermissionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
