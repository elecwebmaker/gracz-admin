import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "app/shared/shared.module";
import { CoreModule } from "app/core/core.module";
import { UserRoutingModule } from "app/user/user.routing";
import { UserListComponent } from './user-list/user-list.component';
import { UserListContainerComponent } from './user-list-container/user-list-container.component';
import { UserListPageComponent } from './user-list-page/user-list-page.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserAddPageComponent } from './user-add-page/user-add-page.component';
import { UserAddFormContainerComponent } from './user-add-form-container/user-add-form-container.component';
import { UserSenderService } from './user-sender.service';
import { UserEditPageComponent } from './user-edit-page/user-edit-page.component';
import { UserEditFormContainerComponent } from './user-edit-form-container/user-edit-form-container.component';
import { UserGetGetterService } from './user-get-getter.service';
import { UserGetterService } from './user-getter.service';
import { UserCardComponent } from './user-card/user-card.component';
import { UserArchivedListPageComponent } from './user-archived-list-page/user-archived-list-page.component';
import { UserArchivedListContainerComponent } from './user-archived-list-container/user-archived-list-container.component';
import { UserSearchbarComponent } from './user-searchbar/user-searchbar.component';
import { UserGenPassComponent } from './user-gen-pass/user-gen-pass.component';
import { UserPermissionComponent } from './user-permission/user-permission.component';
import { UserPermissionContainerComponent } from './user-permission-container/user-permission-container.component';
import { UserPermissionPageComponent } from './user-permission-page/user-permission-page.component';
import { UserPermissionPageGetterService } from './user-permission-page-getter.service';
import { UserPermissionDetailComponent } from './user-permission-detail/user-permission-detail.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    UserRoutingModule
  ],
  declarations: [UserListComponent, UserListContainerComponent, UserListPageComponent, UserFormComponent, UserAddPageComponent, UserAddFormContainerComponent, UserEditPageComponent, UserEditFormContainerComponent,UserCardComponent,UserArchivedListPageComponent, UserArchivedListContainerComponent, UserSearchbarComponent, UserGenPassComponent, UserPermissionComponent, UserPermissionContainerComponent, UserPermissionPageComponent, UserPermissionDetailComponent],
  exports: [UserRoutingModule],
  providers: [UserSenderService, UserGetGetterService,UserGetterService,UserPermissionPageGetterService]

})
export class UserModule { }
