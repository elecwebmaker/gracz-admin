import { Component, OnInit, Input } from '@angular/core';
import { UserSenderService } from "app/user/user-sender.service";
import { Router } from "@angular/router";
import { UserListModel } from "app/core/model/user-list.model";
import { UserGetGetterService } from "app/user/user-get-getter.service";

@Component({
  selector: 'app-user-edit-form-container',
  templateUrl: './user-edit-form-container.component.html',
  styleUrls: ['./user-edit-form-container.component.scss']
})
export class UserEditFormContainerComponent implements OnInit {
  prefilldata:UserListModel;
  @Input() user_id: string;
  constructor(private userget_getter: UserGetGetterService,private usersenderservice: UserSenderService,private router: Router) { }

  ngOnInit() {
    this.userget_getter.get(this.user_id).subscribe((res)=>{
      this.prefilldata = res;
    });
  }


  submit(user: UserListModel){
    this.usersenderservice.edit(user).subscribe((res)=>{
       
        this.router.navigate(['/user/list']);
    });
  }
}
