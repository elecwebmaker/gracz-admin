import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditFormContainerComponent } from './user-edit-form-container.component';

describe('UserEditFormContainerComponent', () => {
  let component: UserEditFormContainerComponent;
  let fixture: ComponentFixture<UserEditFormContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEditFormContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditFormContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
