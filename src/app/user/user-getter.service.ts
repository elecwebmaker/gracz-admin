import { Injectable } from '@angular/core';
import { UserListModel } from "app/core/model/user-list.model";
import { CfGetterService, Filterservice } from "craftutility";
import { Observable, Subject } from "rxjs/Rx";
import { Http } from "@angular/http/";
import { UrlProviderService, ACCOUNT_LIST, GEN_PASSWORD } from "app/shared/url-provider.service";

export interface IUserListFilter {
    key?: string;
    job_type?: string;
    archived?: number;
    size?: number;
    offset?: number;
}

@Injectable()
export class UserGetterService extends CfGetterService<UserListModel> {
    filter: Filterservice<IUserListFilter> = new Filterservice<IUserListFilter>();
    resetPage =  new Subject<boolean>();
    constructor(
        private http: Http,
        private urlprovider: UrlProviderService
    ) {
        super();
    }

    listprovider(filter: IUserListFilter): Observable<any> {
        const url = this.urlprovider.getUrl(ACCOUNT_LIST);
        return this.http.post(url, filter).map((res: any) => {
            const result = res.model;
            return {
                model:result.map((item) => {
                    return new UserListModel({
                        id: item.id,
                        sale_mc5_code:item.sale_code,
                        username: item.username,
                        job_type: item.job_type,
                        job_type_text: item.job_type_text,
                        name: item.first_name,
                        lastname: item.last_name,
                        phone: {
                            code: item.phone_code,
                            number: item.phone_number
                        },
                        email: item.email,
                        line_id: item.line_id,
                        pic: {
                         tmp:"",
                         url:item.picture_src
                        },
                        password: undefined,
                        isArchived: filter.archived == 1 ? true : false
                    });
                }),
                total:<number>res.total
            }
            

        });
    }

    genPassword(): Observable<string> {
        const url = this.urlprovider.getUrl(GEN_PASSWORD);
        return this.http.post(url, {}).map((res: any) => {
            return res.password;
        });
    }

}
