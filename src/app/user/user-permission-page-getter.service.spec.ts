import { TestBed, inject } from '@angular/core/testing';

import { UserPermissionPageGetterService } from './user-permission-page-getter.service';

describe('UserPermissionPageGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserPermissionPageGetterService]
    });
  });

  it('should ...', inject([UserPermissionPageGetterService], (service: UserPermissionPageGetterService) => {
    expect(service).toBeTruthy();
  }));
});
