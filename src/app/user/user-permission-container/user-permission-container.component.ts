import { Component, OnInit, Input } from '@angular/core';
import { UserPermissionPageGetterService } from "app/user/user-permission-page-getter.service";
import { UserPermissionPageModel } from "app/core/model/user-permission-page.model";
import { UserSenderService } from "app/user/user-sender.service";
import { Router } from "@angular/router/";

@Component({
    selector: 'app-user-permission-container',
    templateUrl: './user-permission-container.component.html',
    styleUrls: ['./user-permission-container.component.scss']
})
export class UserPermissionContainerComponent implements OnInit {
    @Input() user_id: number;
    public data: UserPermissionPageModel;
    public loading = false;
    constructor(
        private permissionGet: UserPermissionPageGetterService,
        private userSetter: UserSenderService,
        private router: Router
    ) { }

    ngOnInit() {
        this.permissionGet.getPageDetail(this.user_id).subscribe((res) => {
            this.data = new UserPermissionPageModel(res);
        });
    }

    editPermission(list: Array<number>) {
        this.loading = true;
        this.userSetter.setPermission(this.user_id, list).subscribe(() => {
            this.loading = false;
            this.router.navigate(['/user/list']);
        });
    }

}
