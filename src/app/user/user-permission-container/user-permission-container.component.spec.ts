import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionContainerComponent } from './user-permission-container.component';

describe('UserPermissionContainerComponent', () => {
  let component: UserPermissionContainerComponent;
  let fixture: ComponentFixture<UserPermissionContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
