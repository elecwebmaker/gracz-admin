import { Component, OnInit } from '@angular/core';
import { UserListModel } from "app/core/model/user-list.model";
import { UserSenderService } from "app/user/user-sender.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-add-form-container',
  templateUrl: './user-add-form-container.component.html',
  styleUrls: ['./user-add-form-container.component.scss']
})
export class UserAddFormContainerComponent implements OnInit {

  constructor(private usersenderservice: UserSenderService,private router: Router) { }

  ngOnInit() {
  }


  submit(user: UserListModel){
    this.usersenderservice.add(user).subscribe((res)=>{
        this.router.navigate(['/user/list']);
    },
  (err) => {
    alert(JSON.parse(err._body).message);
  });
  }
}
