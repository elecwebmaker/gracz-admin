import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAddFormContainerComponent } from './user-add-form-container.component';

describe('UserAddFormContainerComponent', () => {
  let component: UserAddFormContainerComponent;
  let fixture: ComponentFixture<UserAddFormContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAddFormContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAddFormContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
