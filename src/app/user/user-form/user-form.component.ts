import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { UserListModel } from "app/core/model/user-list.model";
import { validateConfirmPassword } from "app/core/cf-validate-confirm-password";
import { UrlProviderService, UPLOAD_FILE } from "app/shared/url-provider.service";
import { UserGetGetterService } from "app/user/user-get-getter.service";
import { UserSenderService } from "app/user/user-sender.service";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit,OnChanges {

  private formg: FormGroup;
  profile_src:{url,tmp} = {
    url:"",
    tmp:""
  };
  id: any;
  urlupload: string;
  genpassloading: boolean = false;
  @Input() prefildata:UserListModel;
  @Input() is_edit: boolean;
  @Output() submitform: EventEmitter<UserListModel> = new EventEmitter();
  // stillChkCode = false;
  // codemc5valid = false;
  constructor(private _fb: FormBuilder,private urlprovider: UrlProviderService,private usergetgetter:UserGetGetterService,private usersender: UserSenderService) { }
  
  ngOnInit() {
    this.initialform();
    this.urlupload = this.urlprovider.getUrl(UPLOAD_FILE);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.prefildata){
      this.setPrefillData();
    }
  }
  uploadSuccess(res){
    console.log(res,'rrrr');
    this.profile_src = {
      url:res.absolute,
      tmp:res.database
    }
  }

  uploadError(){
    console.log('errror');
  }

  setPrefillData(){
    const prefilldata = this.prefildata;
    this.profile_src = {
      tmp:prefilldata.pic.tmp,
      url:'http://'+ prefilldata.pic.url
    };
    this.id = prefilldata.id;
    this.formg.patchValue({
      username:prefilldata.username,
      type:prefilldata.job_type,
      firstname:prefilldata.name,
      lastname:prefilldata.lastname,
      phone_code:prefilldata.phone.code,
      phone_number:prefilldata.phone.number,
      email:prefilldata.email,
      line_id:prefilldata.line_id,
      salemc5: prefilldata.sale_mc5_code
    });
  }

  initialform(){
    this.formg = this._fb.group({
      username:['', [Validators.required]],
      password:['', [Validators.required],],
      type:['',[Validators.required]],
      firstname:['',[Validators.required]],
      lastname:['',[Validators.required]],
      phone_code:['',[Validators.required,Validators.maxLength(4)]],
      phone_number:['',[Validators.required]],
      email:['',[Validators.email]],
      line_id:[''],
      salemc5:['',[Validators.required]]
    });
    if(this.is_edit){
        this.formg.get('password').setValidators([]);
    }
    // this.formg.get('salemc5').valueChanges.debounceTime(500).distinctUntilChanged().subscribe((val)=>{
    //   this.stillChkCode = true;
    //   this.usersender.chkSaleMc5(val).subscribe((res)=>{
    //     this.stillChkCode = false;
    //     if(res){
    //       this.codemc5valid = true;
    //     }else{
    //       this.codemc5valid = false;
    //     }
    //   },()=>{
    //     this.stillChkCode = false;
    //     this.codemc5valid = false;
    //   });
    // });
    // this.formg.get('password').disable();
    // this.formg.get('repassword').setValidators([<any>Validators.required, validateConfirmPassword(<FormControl>this.formg.get('password'))]);
    // this.formg.get('password').setValidators([<any>Validators.required, validateConfirmPassword(<FormControl>this.formg.get('repassword'),true)]);
  }
  
  genPassword(){
    this.genpassloading = true;
    this.usergetgetter.genPassword().subscribe((pass)=>{
      this.genpassloading = false;
      this.formg.get('password').setValue(pass);
    });
  }

  submit(){
    if(this.formg.valid){
      const formvalue = this.formg.value;
      const userModel = new UserListModel({
        email:formvalue.email,
        id:this.id,
        isArchived:false,
        job_type:formvalue.type,
        line_id:formvalue.line_id,
        name:formvalue.firstname,
        lastname:formvalue.lastname,
        phone:{
          code:formvalue.phone_code,
          number:formvalue.phone_number
        },
        pic:this.profile_src,
        username:formvalue.username,
        password: formvalue.password,
        sale_mc5_code: formvalue.salemc5
      });
      console.log(userModel,'addform');
      this.submitform.next(userModel);
    }
  }
}
