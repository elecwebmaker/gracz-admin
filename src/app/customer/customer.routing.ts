import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerTableContainerComponent } from 'app/customer/customer-table-container/customer-table-container.component';
import { CustomerlistPageComponent } from "app/customer/customerlist-page/customerlist-page.component";
import { CustomerDetailComponent } from "app/customer/customer-detail/customer-detail.component";
import { CustomerExportPageComponent } from "app/customer/customer-export-page/customer-export-page.component";
import { CustomerDetailPageComponent } from "app/customer/customer-detail-page/customer-detail-page.component";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeCustomerGuard } from "app/core/guards/authorize-customer.guard";



const routes: Routes = [
    {
        path: 'customer',
        canActivate:[CfAuthenJWTGuard, AuthorizeCustomerGuard],
        children: [
            {
                path:'',
                redirectTo:'list',
                pathMatch:'full'
            },
            {
                path: 'list',
                component: CustomerlistPageComponent
            }, {
                path: 'export',
                component: CustomerExportPageComponent
            }, {
                path: 'detail/:cus_id',
                component: CustomerDetailPageComponent
            },
            {
                path: '**', redirectTo: '/404'
            }
        ]
    },

    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
})
export class CustomerRoutingModule { }
