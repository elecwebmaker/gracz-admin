import { Component, OnInit, Input } from '@angular/core';
import { CFTableControl } from "craft-table";
import { CustomerGetGetterService } from "app/customer/customer-get-getter.service";

@Component({
  selector: 'app-customer-order-container',
  templateUrl: './customer-order-container.component.html',
  styleUrls: ['./customer-order-container.component.scss']
})
export class CustomerOrderContainerComponent implements OnInit {
  @Input('cus_id') cus_id: string;

control: CFTableControl = new CFTableControl({
        size:10,
        offset:0,
        header: [{
                id: 'order_id',
                label: 'เลขที่เอกสาร'
            },{
                id: 'quantity',
                label: 'จำนวน'
            }, {
                id: 'status_text',
                label: 'ประเภทธุรกิจ'
            }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    datatotal: number = 0;
    constructor(
        private customer_get_getter: CustomerGetGetterService,
    ) { 
       
    }

    ngOnInit() {
        this.customer_get_getter.cus_id = this.cus_id;
        this.observableList = this.customer_get_getter.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });

        this.control.setGetData((offset:number,size:number)=>{
            // this.customer_get_getter.filter.setLockFilter({
            //     size,
            //     offset
            // });
            return this.observableList;
        });
    }

}
