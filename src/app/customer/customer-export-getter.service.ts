import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from 'craftutility/';
import { CustomerExportModel } from 'app/core/model/customer-export.model';
import { UrlProviderService, CUSTOMER_EXPORT_LIST } from 'app/shared/url-provider.service';
import { Http } from '@angular/http/';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Rx';
export interface ICustomerExportFilter {
    key?: string;
    size?:number;
    offset?: number;
}

@Injectable()
export class CustomerExportGetterService extends CfGetterService<CustomerExportModel> {
    filter: Filterservice<ICustomerExportFilter> = new Filterservice<ICustomerExportFilter>();
    resetPage = new Subject();
  constructor(private urlProvider: UrlProviderService, private http: Http) {
      super();
  }

  listprovider(filter: ICustomerExportFilter): Observable<any> {
        const url = this.urlProvider.getUrl(CUSTOMER_EXPORT_LIST);
        return this.http.post(url, filter).map((res: any) => {
            const result = res.model;
            return {
                model: result.map((item) => {
                    return new CustomerExportModel({
                        cus_id: item.id,
                        cus_code: item.customer_code,
                        cus_name: item.name,
                        cus_mc5: item.customer_code_mc52
                    });
                }),
                total:res.total
            }
            
           
        });
    }

}
