import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { UrlProviderService, CUSTOMER_EXPORT_SEND } from "app/shared/url-provider.service";
import { Iexport } from "app/core/Iexport";

@Injectable()
export class CustomerExportSenderService implements Iexport{

  constructor(private http: Http,private urlprovider: UrlProviderService) {

  }

  export(ids:Array<string>){
    const url = this.urlprovider.getUrl(CUSTOMER_EXPORT_SEND);
    const body = {
      customer_id:JSON.stringify(ids.map((id)=>{
        return {
          id:id
        };
      }))
    };

    return this.http.post(url,body);
  }
}
