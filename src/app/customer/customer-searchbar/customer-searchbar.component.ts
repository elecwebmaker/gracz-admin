import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { ICustomerFilter, CustomerGetterService } from "app/customer/customer-getter.service";
import { KeywordListService } from "app/core/keyword-list.service";

@Component({
  selector: 'app-customer-searchbar',
  templateUrl: './customer-searchbar.component.html',
  styleUrls: ['./customer-searchbar.component.scss']
})
export class CustomerSearchbarComponent implements OnInit {
  public filter = new FilterBar<ICustomerFilter>(["key","status"]);
  constructor(private customerGetter: CustomerGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
    this.filter.filterChanges().subscribe((res:any) => {
        this.customerGetter.filter.setLockFilter({
            key:res.key,
            status:res.status
        });
        this.customerGetter.resetPage.next();
    });
    this.filter.setStatusList(
        this.keywordlist.listCustomerStatus()
    );
  }

  searchChange(val) {
      this.filter.setFilter('key', val);
  }

  statusChange(val) {
      this.filter.setFilter('status', val);
  }
}
