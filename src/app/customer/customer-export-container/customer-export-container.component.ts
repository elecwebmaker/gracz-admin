import { Component, OnInit,OnDestroy } from '@angular/core';
import { CFTableControl, CFRow } from "craft-table";
import { ICustomerExportModel } from "app/core/model/customer-export.model";
import { CustomerExportGetterService } from "app/customer/customer-export-getter.service";
import { Subscription, Subject } from 'rxjs/Rx';
import { Router } from '@angular/router';
@Component({
    selector: 'app-customer-export-container',
    templateUrl: './customer-export-container.component.html',
    styleUrls: ['./customer-export-container.component.scss']
})
export class CustomerExportContainerComponent implements OnInit,OnDestroy {

    subscription:Subscription;
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }

    public chk_data = [];
    control: CFTableControl = new CFTableControl({
        header: [
        {
            id: '@checkbox',
            label: 'ทั้งหมด'
        },
        {
            id: 'cus_code',
            label: 'ID ลูกค้า'
        }, {
            id: 'cus_name',
            label: 'ชื่อลูกค้า'
        }, {
            id: 'cus_mc5',
            label: ''
        }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    isSetObservable = false;
    datatotal: number = 10;
    constructor(private router : Router, private customerExportGetterService: CustomerExportGetterService) {
        this.observableList =  this.customerExportGetterService.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });
    }


    refresh(){
        this.control.refresh();
        this.chk_data = [];
    }

    ngOnInit() {
        this.control.setGetData((offset: number, size: number) => {
            
            this.customerExportGetterService.filter.setLockFilter({
                size,
                offset
            });
            return this.observableList;
        });
        this.subscription = this.customerExportGetterService.resetPage.subscribe(()=>{
            this.currentPage = 1;
        });
    }

    onCheckbox(item: Array<CFRow>) {
        this.chk_data = item.map((row)=>{
            return row.get('cus_id').getValue();
        })
    }

    viewDetail(cfrow: {row:CFRow}){
        const cus_id = cfrow.row.get('cus_id').getValue();
        this.router.navigate([`/customer/detail/${cus_id}`])
    }
}
