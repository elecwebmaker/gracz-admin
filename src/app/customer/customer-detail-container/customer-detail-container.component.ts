import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CfIContainerComp } from "app/shared/cf-comp-dispatcher/IcontainerComp";
import { CustomerTableModel } from "app/core/model/customer-table.model";
import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";
import { CustomerDetailModel } from "app/core/model/customer-detail.model";
import { CustomerGetGetterService } from "app/customer/customer-get-getter.service";

@Component({
  selector: 'app-customer-detail-container',
  templateUrl: './customer-detail-container.component.html',
  styleUrls: ['./customer-detail-container.component.scss']
})
export class CustomerDetailContainerComponent implements OnInit, CfIContainerComp<CustomerDetailModel> {
  @Input('cus_id') cus_id:string;
  @Output() button: EventEmitter<any> = new EventEmitter();
  data_dispatcher: CfDataDispatcher<CustomerDetailModel> = new CfDataDispatcher<CustomerDetailModel>();
  constructor(
   private customergetGetter: CustomerGetGetterService
  ) { }
  
  ngOnInit() {
    
    this.customergetGetter.get(this.cus_id).subscribe((data)=>{
        this.button.emit(data.button);
      this.data_dispatcher.setData(data);
    })
  }
}
