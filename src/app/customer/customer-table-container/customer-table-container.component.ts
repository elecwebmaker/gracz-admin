import { Component, OnInit,OnDestroy } from '@angular/core';
import { CFTableControl, CFRow } from 'craft-table/';
import { CustomerTableModel, ICustomerTableModel } from '../../core/model/customer-table.model';
import { CustomerGetterService } from "app/customer/customer-getter.service";
import { Router } from "@angular/router";
import { Subscription, Subject } from 'rxjs/Rx';
@Component({
    selector: 'app-customer-table-container',
    templateUrl: './customer-table-container.component.html',
    styleUrls: ['./customer-table-container.component.scss']
})
export class CustomerTableContainerComponent implements OnInit,OnDestroy {
    subscription:Subscription;
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }
    control: CFTableControl = new CFTableControl({
        header: [{
                id: 'cus_code',
                label: 'รหัสลูกค้า'
            },{
                id: 'cus_name',
                label: 'ชื่อลูกค้า'
            }, {
                id: 'business_type',
                label: 'ประเภทธุรกิจ'
            }, {
                id: 'sale_name',
                label: 'พนักงานขาย'
            }, {
                id: 'status_text',
                label: 'สถานะ'
            }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    datatotal: number = 0;
    constructor(
        private customergetter: CustomerGetterService,
        private router : Router
    ) {
        this.observableList = this.customergetter.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });
    }

    ngOnInit() {
            
        this.control.setGetData((offset:number,size:number)=>{
            
            this.customergetter.filter.setLockFilter({
                size,
                offset
            });
            return this.observableList;
        });
        this.subscription = this.customergetter.resetPage.subscribe(()=>{
            this.currentPage = 1;
        });
    }
    viewDetail(cfrow: {row:CFRow}){
        const cus_id = cfrow.row.get('cus_id').getValue();
        this.router.navigate([`/customer/detail/${cus_id}`])
    }
}