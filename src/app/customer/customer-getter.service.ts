import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from 'craftutility';
import { Observable } from "rxjs/Rx";
import { Http } from "@angular/http";
import { UrlProviderService, CUSTOMER_LIST } from "app/shared/url-provider.service";
import { CustomerTableModel } from "app/core/model/customer-table.model";
import { Subject } from 'rxjs/Rx';
export interface ICustomerFilter{
  status?:string;
  key?: string;
  size?: number;
  offset?: number;
}

@Injectable()
export class CustomerGetterService extends CfGetterService<CustomerTableModel>{
  filter: Filterservice<ICustomerFilter> = new Filterservice<ICustomerFilter>();
  resetPage = new Subject();
  listprovider(filter:ICustomerFilter): Observable<any> {
    const url = this.urlprovider.getUrl(CUSTOMER_LIST);
    return this.http.post(url,filter).map((res:any)=>{
      const result = res.model;
      return {
        model: result.map((item)=>{
          return new CustomerTableModel({
            business_type:item.business_type_text,
            cus_id:item.id,
            cus_code:item.customer_code,
            cus_name:item.customer_name,
            sale_name:item.sale_name,
            status_text:item.status_text
          })
        }),
        total:res.total
      }

    });
  }

  constructor(private http: Http,private urlprovider: UrlProviderService) {
    super();
  }

}
