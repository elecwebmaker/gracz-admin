import { Injectable } from "@angular/core";
import {
  UrlProviderService,
  CUSTOMER_SET_STATUS
} from "app/shared/url-provider.service";
import { Http } from "@angular/http/";
import { Observable } from "rxjs/Rx";

export interface ICustomerSetStatusModel {
  customer_id: number;
  status: number;
  customer_code_mc5?: string;
}

@Injectable()
export class CustomerStatusSetterService {
  constructor(private urlProvider: UrlProviderService, private http: Http) {}

  setStatus(body: ICustomerSetStatusModel): Observable<boolean> {
    const url = this.urlProvider.getUrl(CUSTOMER_SET_STATUS);
    return this.http.post(url, body).map((res: any) => {
      return res.success;
    });
  }
}
