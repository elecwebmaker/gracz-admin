import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SemanticModalComponent } from 'ng-semantic/ng-semantic';
import { NotifyService } from 'craftutility';
import { Router } from "@angular/router";
import { CustomerStatusSetterService, ICustomerSetStatusModel } from "app/customer/customer-status-setter.service";
@Component({
    selector: 'app-set-status',
    templateUrl: './set-status.component.html',
    styleUrls: ['./set-status.component.scss']
})
export class SetStatusComponent implements OnInit {
    @ViewChild('typePasswordModel') typePasswordModel: SemanticModalComponent;
    @Input() cus_id;
    @Input() data: Array<{id: number, text: string}>;
    isCompleted: boolean = true;
    pass: string;
    submitted = false;
    constructor(
        private notiservice: NotifyService,
        private router: Router,
        private customerStatusSetterService: CustomerStatusSetterService
    ) { }

    ngOnInit() {
    }

    approve() {
        this.typePasswordModel.show();
    }

    approve_submit(is_valid) {
        this.submitted = true;
        if (is_valid) {
            this.typePasswordModel.hide();
            this.setStatus(1, this.pass).subscribe();
        }
    }

    // edit() {
    //     this.setStatus(4).subscribe();
    // }

    reject() {
        this.setStatus(2).subscribe();
    }

    private setStatus(status: number, mc5?: string) {
        this.isCompleted = false;
        const body: ICustomerSetStatusModel = {
            customer_id: this.cus_id,
            customer_code_mc5: mc5,
            status: status
        }
        return this.customerStatusSetterService.setStatus(body).map(() => {
            this.notiservice.showSuccess();
            this.isCompleted = true;
            this.router.navigate(['/customer/list']);
        });
    }
}
