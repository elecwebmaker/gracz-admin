import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ICustomerDetailModel, CustomerDetailModel } from "app/core/model/customer-detail.model";
import { CfPresentComponent } from "craftutility";

@Component({
    selector: 'app-customer-detail',
    templateUrl: './customer-detail.component.html',
    styleUrls: ['./customer-detail.component.scss'],
    providers:[
         {
            provide:CfPresentComponent,
            useExisting: forwardRef(()=>CustomerDetailComponent)
        }
    ]
})
export class CustomerDetailComponent extends CfPresentComponent<CustomerDetailModel>{

    constructor() {
        super();
    }

    openFile(ref) {
        const url = 'http://' + ref;
        window.open(url);
    }

    ngOnChanges() {
        console.log(this.data);
    }

}
