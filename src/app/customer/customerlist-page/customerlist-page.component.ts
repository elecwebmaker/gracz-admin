import { Component, OnInit } from '@angular/core';
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-customerlist-page',
  templateUrl: './customerlist-page.component.html',
  styleUrls: ['./customerlist-page.component.scss']
})
export class CustomerlistPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
