import { Component, OnInit } from '@angular/core';
import { CFTableControl } from "craft-table/";

@Component({
    selector: 'app-customer-history',
    templateUrl: './customer-history.component.html',
    styleUrls: ['./customer-history.component.scss']
})
export class CustomerHistoryComponent implements OnInit {
    control: CFTableControl = new CFTableControl({
        header: [{
            id: 'doc_no',
            label: 'เลขที่เอกสาร'
        }, {
            id: 'order_qty',
            label: 'จำนวนรายการ'
        }, {
            id: 'status',
            label: 'สถานะ'
        }],
        navigating: false
    });
    constructor() { }

    ngOnInit() {
    }

}
