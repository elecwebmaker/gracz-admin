import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer.routing';
import { CustomerTableContainerComponent } from './customer-table-container/customer-table-container.component';
import { SharedModule } from 'app/shared/shared.module';
import { CustomerlistPageComponent } from './customerlist-page/customerlist-page.component';
import { CoreModule } from "app/core/core.module";
import { CustomerGetterService } from './customer-getter.service';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerHistoryComponent } from './customer-history/customer-history.component';
import { CustomerExportContainerComponent } from './customer-export-container/customer-export-container.component';
import { CustomerExportPageComponent } from './customer-export-page/customer-export-page.component';
import { CustomerDetailPageComponent } from './customer-detail-page/customer-detail-page.component';
import { CustomerDetailContainerComponent } from './customer-detail-container/customer-detail-container.component';
import { CustomerGetGetterService } from './customer-get-getter.service';
import { CustomerOrderContainerComponent } from './customer-order-container/customer-order-container.component';
import { SetStatusComponent } from './set-status/set-status.component';
import { CustomerSearchbarComponent } from './customer-searchbar/customer-searchbar.component';
import { CustomerExportSearchbarComponent } from './customer-export-searchbar/customer-export-searchbar.component';
import { CustomerExportGetterService } from './customer-export-getter.service';
import { CustomerExportSenderService } from './customer-export-sender.service';
import { CustomerStatusSetterService } from './customer-status-setter.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CustomerRoutingModule,
    CoreModule,
  ],
  declarations: [CustomerTableContainerComponent, CustomerlistPageComponent, CustomerDetailComponent, CustomerHistoryComponent, CustomerExportContainerComponent, CustomerExportPageComponent, CustomerDetailPageComponent, CustomerDetailContainerComponent, CustomerOrderContainerComponent, SetStatusComponent, CustomerSearchbarComponent, CustomerExportSearchbarComponent],
  exports: [CustomerRoutingModule],
  providers: [CustomerGetterService, CustomerGetGetterService, CustomerExportGetterService, CustomerExportSenderService,CustomerStatusSetterService]

})
export class CustomerModule { }
