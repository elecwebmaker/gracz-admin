import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-customer-detail-page',
    templateUrl: './customer-detail-page.component.html',
    styleUrls: ['./customer-detail-page.component.scss']
})
export class CustomerDetailPageComponent implements OnInit {
    cus_id: string;
    button: Array<{ id: number, text: string }>;
    mapParams:Object;
    constructor(
        private activatedRouter: ActivatedRoute
    ) { }

    ngOnInit() {
        this.cus_id = this.activatedRouter.snapshot.params['cus_id'];
        this.mapParams = {
            customerdetail:{
                id:this.cus_id
            }
        }
    }

    setButton(button) {
        this.button = button;
    }

}
