import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerExportContainerComponent } from "app/customer/customer-export-container/customer-export-container.component";
import { CustomerExportSenderService } from "app/customer/customer-export-sender.service";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-customer-export-page',
  templateUrl: './customer-export-page.component.html',
  styleUrls: ['./customer-export-page.component.scss']
})
export class CustomerExportPageComponent implements OnInit {
  @ViewChild('container') customer_container: CustomerExportContainerComponent;
  constructor(public customer_export: CustomerExportSenderService) { }

  ngOnInit() {
  }

  states:CfIbreadState[] = [
    {
      name:"export",
      text:"รายการส่งออก",
      url:"/customer/export"
    } 
  ];

  exportSuccess(isSuccess){
    if(isSuccess){
      this.customer_container.refresh();
    }
  }
}
