import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { UrlProviderService, CUSTOMER_GET, ORDER_LIST_BY_CUSTOMER } from "app/shared/url-provider.service";
import { CustomerDetailModel } from "app/core/model/customer-detail.model";
import { Observable } from "rxjs/Rx";
import { CustomerOrderModel } from "app/core/model/customer-order.model";
import { CfGetterService, Filterservice } from 'craftutility';

@Injectable()
export class CustomerGetGetterService extends CfGetterService<any>{
  filter: Filterservice<any> = new Filterservice<any>();
  get(cus_id):Observable<CustomerDetailModel>{
    const url = this.urlprovider.getUrl(CUSTOMER_GET);
    return this.http.post(url + '/id/'+ cus_id,{}).map((res:any)=>{
      const item = res.model;
      return new CustomerDetailModel({
        bill_addr:item.billing_address.text,
        bill_condition:item.billing_condition,
        businessType:item.business_type_text,
        cheque_condition:item.cheque_condition,
        code:item.customer_code,
        credit_expected:item.expect_credit_amount,
        month_expect: item.expect_per_month,
        files:item.files,
        name:item.name,
        payment_method:item.payment_text,
        registered_date: item.register_date,
        remark: item.remark ,
        sale_code: item.sale_code,
        sale_discount:item.discount_text ,
        ship_addrs:item.shipment_address.map((addr)=>{
          return addr.text
        }),
        tel:(item.phone_number.map((phone)=>{
          return `+${phone.code}${phone.number}`;
        })),
        id_card:item.id_card,
        button: item.button
      });
      

  
    });
  }
  cus_id: string;
  listprovider(filter:any):any{
    const url = this.urlprovider.getUrl(ORDER_LIST_BY_CUSTOMER);
    return this.http.post(url + '/cid/'+ this.cus_id,{}).map((res:any)=>{
      const result = res.model;
      const model = result.map((item)=>{
        return new CustomerOrderModel({
          order_id: item.doc_no,
          quantity: item.list_quantity,
          status_text: item.status_text
        })
      });
      return {
        model,
        total:res.total
      }
    });
  }

  constructor(private http: Http,private urlprovider: UrlProviderService) {
   super();
  }


}
