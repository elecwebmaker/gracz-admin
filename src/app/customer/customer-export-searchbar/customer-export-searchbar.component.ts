import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { CustomerExportGetterService, ICustomerExportFilter } from "app/customer/customer-export-getter.service";
import { KeywordListService } from "app/core/keyword-list.service";

@Component({
  selector: 'app-customer-export-searchbar',
  templateUrl: './customer-export-searchbar.component.html',
  styleUrls: ['./customer-export-searchbar.component.scss']
})
export class CustomerExportSearchbarComponent implements OnInit {
  public filter = new FilterBar<ICustomerExportFilter>(["key"]);
  constructor(private modelgetter: CustomerExportGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
    this.filter.filterChanges().subscribe((res:any) => {
        this.modelgetter.filter.setLockFilter({
            key:res.key,
        });
        this.modelgetter.resetPage.next();
    });
  }

  searchChange(val) {
      this.filter.setFilter('key', val);
  }

}
