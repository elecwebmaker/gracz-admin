import { Component } from '@angular/core';
import { CFAuthenJWTManageService } from 'app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service';
import {CraftHttpNotifyService } from 'craftutility';
import { UserAuthenService } from 'app/core/user-authen.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private router: Router,private cfAuthenMange: CFAuthenJWTManageService,private userAuthen:UserAuthenService, private crafthttpnotify: CraftHttpNotifyService) {
        this.cfAuthenMange.setRouteWhenExpired('/login');
        this.cfAuthenMange.setRouteWhenInvalid('/login');
        this.cfAuthenMange.setRouteWhenUnAuthorize('/profile');
        this.crafthttpnotify.getError().subscribe((err:any)=>{
            if(err){
                if(err.status == 403 || err.status == 401){
                    this.userAuthen.logout().subscribe(()=>{
                        this.router.navigate(['/login']);
                    });
                }
            }
        });
    }
}
