import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from "craftutility";
import { UrlProviderService, INVENTORY_LIST } from "app/shared/url-provider.service";
import { Http } from "@angular/http";
import { Observable, Subject } from "rxjs/Rx";
import { InventoryTableModel } from "app/core/model/inventory-table.model";

export interface IInventoryFilter{
  category?: string;
  key?: string;
  size?: number;
  offset?: number
}

@Injectable()
export class InventoryGetterService extends CfGetterService<InventoryTableModel>{
  filter: Filterservice<IInventoryFilter> = new Filterservice<IInventoryFilter>();
  resetPage = new Subject();
  listprovider(filter:IInventoryFilter): Observable<any> {
    const url = this.urlprovider.getUrl(INVENTORY_LIST);
    return this.http.post(url,filter).map((res:any)=>{
      const result = res.model;
      return {
        model: result.map((item)=>{
          return new InventoryTableModel({
              fgcode:item.product_code,
              order_qty: item.order_qty,
              product_name: item.name,
              remain_qty: item.remain,
              sale_qty: item.sale_qty,
              unit: item.unit
          });
        }),
        total:res.total
      }
    });
  }

  constructor(private http: Http,private urlprovider: UrlProviderService) {
    super();
  }

}
