import { Component, OnInit, OnDestroy } from '@angular/core';
import { CFTableControl } from "craft-table/";
import { IInventoryTableModel } from "app/core/model/inventory-table.model";
import { InventoryGetterService } from './../inventory-getter.service';
import { Subscription } from 'rxjs/Rx';
@Component({
    selector: 'app-inventory-table-container',
    templateUrl: './inventory-table-container.component.html',
    styleUrls: ['./inventory-table-container.component.scss']
})
export class InventoryTableContainerComponent implements OnInit,OnDestroy {
    subscription:Subscription;
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }

    control: CFTableControl = new CFTableControl({
        header: [{
            id: 'fgcode',
            label: 'รหัส FG'
        }, {
            id: 'product_name',
            label: 'ชื่อสินค้า'
        }, {
            id: 'order_qty',
            label: 'จำนวนสั่งซื้อ'
        }, {
            id: 'sale_qty',
            label: 'จำนวนสั่งขาย'
        }, {
            id: 'remain_qty',
            label: 'จำนวนคงเหลือ'
        }, {
            id: 'unit',
            label: 'หน่วย'
        }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    datatotal: number = 0;
    constructor(private inventorygetter: InventoryGetterService) {
        this.observableList =  this.inventorygetter.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });
    }
    
    ngOnInit() {
        this.control.setGetData((offset: number, size: number)=>{
            this.inventorygetter.filter.setLockFilter({
                size,
                offset
            });
            
            return this.observableList;
        });
        this.subscription = this.inventorygetter.resetPage.subscribe(()=>{
            this.currentPage = 1;
        });
    }

}
