import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { KeywordListService } from "app/core/keyword-list.service";
import { IInventoryFilter, InventoryGetterService } from "app/inventory/inventory-getter.service";

@Component({
  selector: 'app-inventory-searchbar',
  templateUrl: './inventory-searchbar.component.html',
  styleUrls: ['./inventory-searchbar.component.scss']
})
export class InventorySearchbarComponent implements OnInit {
  public filter = new FilterBar<IInventoryFilter>(["key","category"]);
  constructor(private modelgetter: InventoryGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
    this.filter.filterChanges().subscribe((res:any) => {
        this.modelgetter.filter.setLockFilter({
            key:res.key,
            category:res.category
        });
        this.modelgetter.resetPage.next();
    });
    this.filter.setStatusList(
        this.keywordlist.listInventoryUnit()
    );
  }

  searchChange(val) {
      this.filter.setFilter('key', val);
  }

  statusChange(val) {
      this.filter.setFilter('category', val);
  }
}
