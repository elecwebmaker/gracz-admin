import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryTableContainerComponent } from './inventory-table-container/inventory-table-container.component';
import { InventoryRoutingModule } from "app/inventory/inventory.routing";
import { SharedModule } from "app/shared/shared.module";
import { InventoryTablePageComponent } from './inventory-table-page/inventory-table-page.component';
import { CoreModule } from "app/core/core.module";
import { InventorySearchbarComponent } from './inventory-searchbar/inventory-searchbar.component';
import { InventoryGetterService } from './inventory-getter.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InventoryRoutingModule,
    CoreModule
  ],
  declarations: [InventoryTableContainerComponent, InventoryTablePageComponent, InventorySearchbarComponent],
  exports: [InventoryRoutingModule],
  providers: [InventoryGetterService]
})
export class InventoryModule { }
