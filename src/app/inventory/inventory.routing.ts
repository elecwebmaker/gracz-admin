import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryTablePageComponent } from 'app/inventory/inventory-table-page/inventory-table-page.component';
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeInventoryGuard } from "app/core/guards/authorize-inventory.guard";



const routes: Routes = [
    {
        path: 'inventory',
        canActivate:[CfAuthenJWTGuard, AuthorizeInventoryGuard],
        children: [
            {
                path:'',
                redirectTo:'list',
                pathMatch:'full'
            },
            {
                path: 'list',
                component: InventoryTablePageComponent
            },
            {
                path: '**', redirectTo: '/404'
            }
        ]
    },
    // {
    //     path: '**', redirectTo: '/404'
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
})
export class InventoryRoutingModule { }
