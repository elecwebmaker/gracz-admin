import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-product-list-page',
  templateUrl: './product-list-page.component.html',
  styleUrls: ['./product-list-page.component.scss']
})
export class ProductListPageComponent implements OnInit {
  model_id: string;
  unit: string;

  mapParams:object
  constructor(
    private activatedroute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.model_id = this.activatedroute.snapshot.params['model_id'];
    this.unit = this.activatedroute.snapshot.params['unit'];
    this.mapParams = {
      productlist:{
        model_id:this.model_id,
        unit_type:this.unit,
      }
    };

  }
  setBreadCrumbData(data:{model_code}){
    this.mapParams = {
      ...this.mapParams,
      ...{
        productlist:{
          model_id:this.model_id,
          unit_type:this.unit,
          model_code: data.model_code
        }
      }
    }
  }
}
