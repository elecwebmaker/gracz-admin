import { Injectable } from '@angular/core';
import { UrlProviderService, ORDER_EXPORT_SEND } from "app/shared/url-provider.service";
import { Http } from "@angular/http";

@Injectable()
export class OrderExportSenderService {

  constructor(private http: Http,private urlprovider: UrlProviderService) {

  }

  export(ids:Array<string>){
    const url = this.urlprovider.getUrl(ORDER_EXPORT_SEND);
    const body = {
      order_id:JSON.stringify(ids.map((id)=>{
        return {
          id:id
        };
      }))
    };

    return this.http.post(url,body);
  }

}
