import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from 'craftutility';
import { ProductCardModel } from "app/core/model/product-card.model";
import { Observable, Subject } from "rxjs/Rx";
import { Http } from "@angular/http";
import { UrlProviderService, PRODUCT_LIST } from "app/shared/url-provider.service";

export interface ProductListFilter {
  key?: string;
  unit?: string;
  status?: string;
  model_id?: string;
  size?: number;
  offset?: number;
}
@Injectable()
export class ProductGetterService extends CfGetterService<ProductCardModel>{
  filter: Filterservice<ProductListFilter> = new Filterservice<ProductListFilter>();
  model_code: string;
  resetPage: Subject<any> = new Subject<any>();
  listprovider(filter:ProductListFilter): Observable<any> {
    const url = this.urlprovider.getUrl(PRODUCT_LIST);
    return this.http.post(url,filter).map((res:any)=>{
      const result = res.model;
      this.model_code = res.model_text;
      return {
        model:result.map((item)=>{
          return new ProductCardModel({
            cus_name:item.customer_name,
            product_code:item.product_code,
            doc_no:item.doc_no,
            quantity_text:item.quantity_text,
            price_text:item.price_text,
            status_text:item.status_text,
            default_price_text:item.default_price_text,
            total_price_text:item.total_price_text,
            oid: item.order_id,
            pid: item.product_id
          });
        }),
        total:res.total ? res.total : 50
      }
      

    });
  }

  constructor(private http: Http,private urlprovider: UrlProviderService) {
    super();
  }

}
