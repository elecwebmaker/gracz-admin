import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { ProductCardModel,IProductCardModel } from './../../core/model/product-card.model';
@Component({
    selector: 'app-product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnChanges {
    @Input() data: ProductCardModel | undefined;
    @Input() loading: boolean;
    public statusColor;
    constructor() { }

    ngOnChanges() {
        if (this.data) {
            this.statusColor = {
                positive: this.data.status_text === 'อนุมัติ',
                negative: this.data.status_text === 'ไม่อนุมัติ',
                warning: this.data.status_text === 'รออนุมัติ'
            };
        }
    }

}
