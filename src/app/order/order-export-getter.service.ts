import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from 'craftutility/';
import { OrderExportModel } from 'app/core/model/order-export.model';
import { Observable, Subject } from 'rxjs/Rx';
import { UrlProviderService, ORDER_EXPORT_LIST } from 'app/shared/url-provider.service';
import { Http } from '@angular/http/';

export interface IOrderExportFilter{
    key?: string;
    size?: number;
    offset?: number;
}

@Injectable()
export class OrderExportGetterService extends CfGetterService<OrderExportModel> {
  filter: Filterservice<IOrderExportFilter> = new Filterservice<IOrderExportFilter>();
  resetPage = new Subject();
  constructor(private urlprovider: UrlProviderService, private http: Http) {
      super();
  }

  listprovider(filter: IOrderExportFilter): Observable<any> {
        const url =this.urlprovider.getUrl(ORDER_EXPORT_LIST);
        return this.http.post(url,filter).map((res: any) => {
            const result = res.model;
            const model = result.map((item) => {
                return new OrderExportModel({
                    order_id: item.order_id,
                    preorder_id: item.preorder_id,
                    customer_code: item.customer_code,
                    customer_name: item.customer_name,
                    shipment_date: item.shipment_date,
                    total_price: item.total_price,
                    product_id: item.product_id
                });
            });
            return {
                model,
                total:res.total
            }
        });
    }

}
