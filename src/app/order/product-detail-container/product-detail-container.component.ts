import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CfIContainerComp } from 'app/shared/cf-comp-dispatcher/IcontainerComp';
import { CfDataDispatcher } from 'app/shared/cf-comp-dispatcher/cf-data-dispatcher';
import { OrderProductModel } from 'app/core/model/order-product.model';
import { ProductGetGetterService } from 'app/order/product-get-getter.service';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail-container',
  templateUrl: './product-detail-container.component.html',
  styleUrls: ['./product-detail-container.component.scss']
})
export class ProductDetailContainerComponent implements OnInit,CfIContainerComp<OrderProductModel> {
  data_dispatcher: CfDataDispatcher<OrderProductModel> = new CfDataDispatcher<OrderProductModel>();
  @Input() product_id: string;
  @Input() order_id: string;
  @Output() getBreadCrumbData: EventEmitter<{model_code:string,unit:string, model_id:string,pcode: string}> = new EventEmitter();
  constructor(
    private productget_getter: ProductGetGetterService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.params.subscribe((params) => {
      this.initial(params['product_id']);
      this.product_id = params['product_id'];
    });
  }

  initial(p_id) {
    this.productget_getter.get(this.order_id, p_id).subscribe((res)=> {
        this.getBreadCrumbData.next({
          model_code: res.model_code,
          model_id: res.model_id,
          pcode: res.product_card_detail.product_code,
          unit: res.unit
        });
        this.data_dispatcher.setData(res);
    });
  }


}
