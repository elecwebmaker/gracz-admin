import { Injectable } from '@angular/core';
import { UrlProviderService, PRODUCT_SET_STATUS } from 'app/shared/url-provider.service';
import { Http } from '@angular/http/';
import { Observable } from 'rxjs/Rx';

export interface IProductSetStatusModel {
    product_id: number;
    status: number;
}

@Injectable()
export class ProductStatusSetterService {

    constructor(private urlProviderService: UrlProviderService, private http: Http) { }

    setStatus(body: IProductSetStatusModel): Observable<boolean> {
        const url = this.urlProviderService.getUrl(PRODUCT_SET_STATUS);
        return this.http.post(url, body).map((res: any) => {
            return res.success;
        });
    }
}
