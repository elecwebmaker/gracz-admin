import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
  UrlProviderService,
  ORDER_PRODUCT_GET
} from 'app/shared/url-provider.service';
import { Observable } from 'rxjs/Rx';
import { OrderProductModel } from 'app/core/model/order-product.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ProductGetGetterService {
  control$: Subject<boolean> = new Subject<boolean>();
  constructor(private http: Http, private urlprovider: UrlProviderService) {}

  get(order_id, product_id): Observable<OrderProductModel> {
    const url = this.urlprovider.getUrl(ORDER_PRODUCT_GET);
    return this.control$.startWith(true).flatMap(() => {
      return this.http
        .post(url, {
          order_id: order_id,
          product_id: product_id
        })
        .map((res: any) => {
          const result = res.model;
          return new OrderProductModel({
            invoice_qty: result.invoice_qty,
            product_card_detail: {
              category: result.category_text,
              model: result.model_text,
              unit: result.unit_text,
              quantity: result.quantity_text,
              default_price: result.default_price,
              price: result.price,
              discount_per: result.discount_percent_text,
              discount_val: result.discount_value_text,
              total_price: result.total_price,
              screen: result.screen == 1 ? 'สกรีนกล่อง' : 'ไม่สกรีนกล่อง',
              doc_no: result.doc_no,
              customer_name: result.customer_name,
              status: result.status,
              status_text: result.status_text,
              product_code: result.product_code
            },
            model_code: result.model_text,
            model_id: result.model_id,
            unit: result.unit_text,
            product_quantity: result.product_quantity,
            phone_number: result.phone_number.map((res: any) => {
              return res.phone_number_text;
            }),
            remark: result.remark,
            register_date: result.register_date,
            register_time: result.register_time,
            shipment_address: result.shipment_address,
            shipment_date: result.shipment_date,
            SO_qty: result.SO_qty,
            total_price_text: result.total_price_text,
            product_table: result.product_list.map(product => {
              return {
                name: product.name,
                product_code: product.product_code,
                quantity: product.quantity,
                unit: product.unit_text,
                price: product.price_text,
                product_id: product.product_id,
                status_text: product.status_text
              };
            }),
            button: result.button.map(item => {
              return {
                id: item.id,
                text: item.text
              };
            })
          });
        });
    });
  }

  refresh() {
    this.control$.next(true);
  }
}
