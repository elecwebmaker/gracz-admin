import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-order-export-button',
    templateUrl: './order-export-button.component.html',
    styleUrls: ['./order-export-button.component.scss']
})
export class OrderExportButtonComponent implements OnInit {
    @Input() toSubmit: Array<number> = [];
    constructor() { }

    ngOnInit() {
    }

    onSubmit() {
        console.log(this.toSubmit);
    }
}
