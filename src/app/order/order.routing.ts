import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from 'app/order/product-list/product-list.component';
import { ProductListPageComponent } from "app/order/product-list-page/product-list-page.component";
import { ProductCardContainerComponent } from "app/order/product-card-container/product-card-container.component";
import { ProductDetailComponent } from "app/order/product-detail/product-detail.component";
import { ModelListPageComponent } from "app/order/model-list-page/model-list-page.component";
import { ProductDetailPageComponent } from "app/order/product-detail-page/product-detail-page.component";
import { OrderExportPageComponent } from "app/order/order-export-page/order-export-page.component";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { AuthorizeOrderGuard } from "app/core/guards/authorize-order.guard";



const routes: Routes = [
    {
        path:'model',
        canActivate:[CfAuthenJWTGuard, AuthorizeOrderGuard],
        children:[
            {
                path:'',
                redirectTo:'list',
                pathMatch:'full'
            },
            {
                path: 'list',
                component: ModelListPageComponent,
            },
            {
                path:'product',
                canActivate:[CfAuthenJWTGuard, AuthorizeOrderGuard],
                children:[
                    {
                        path: ':model_id/:unit/list',
                        component: ProductListPageComponent,
                    }, {
                        path: 'detail/:order_id/:product_id',
                        component: ProductDetailPageComponent
                    }, {
                        path: 'export',
                        component: OrderExportPageComponent
                    }, {
                        path: '**', redirectTo: '/404'
                    }
                ]
            },
            {
                path: '**', redirectTo: '/404'
            }
        ]
    },
   
    // {
    //     path: '**', redirectTo: '/404'
    // }
    
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
})
export class OrderRoutingModule { }
