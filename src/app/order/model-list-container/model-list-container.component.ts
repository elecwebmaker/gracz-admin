import { Component, OnInit } from '@angular/core';
import { CFTableControl, CFRow } from "craft-table";
import { ModelGetterService } from "app/order/model-getter.service";
import { Router } from "@angular/router";
import { Subscription, Subject } from 'rxjs/Rx';
@Component({
  selector: 'app-model-list-container',
  templateUrl: './model-list-container.component.html',
  styleUrls: ['./model-list-container.component.scss']
})
export class ModelListContainerComponent implements OnInit {
    subscription:Subscription;
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }
    control: CFTableControl = new CFTableControl({
        header: [{
                id: 'model_name',
                label: 'โมเดล'
            },{
                id: 'total_order',
                label: 'ผลรวมทั้งหมด'
            }, {
                id: 'quantity',
                label: 'จำนวนในคลังคงเหลือ'
            }, {
                id: 'unit',
                label: 'หน่วย'
            }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    datatotal = 0;
    constructor(private modelgetter: ModelGetterService,private router: Router) {
        this.observableList =  this.modelgetter.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });
    }

    ngOnInit() {
       this.control.setGetData((offset:number,size:number)=>{
        this.modelgetter.filter.setLockFilter({
            size,
            offset
        });
        return this.observableList
       });
       this.subscription = this.modelgetter.resetPage.subscribe(()=>{
            this.currentPage = 1;
       });
    }
    showOrderProduct(cfrow: {row:CFRow}){
        const model_id = cfrow.row.get('model_id').getValue();
        const unit = cfrow.row.get('unit').getValue();
        this.router.navigate([`/model/product/${model_id}/${unit}/list`])
    }

}
