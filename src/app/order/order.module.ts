import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductListComponent } from './product-list/product-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { OrderRoutingModule } from 'app/order/order.routing';
import { ProductListPageComponent } from './product-list-page/product-list-page.component';
import { ProductCardContainerComponent } from './product-card-container/product-card-container.component';
import { CoreModule } from "app/core/core.module";
import { ProductGetterService } from './product-getter.service';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ModelListComponent } from './model-list/model-list.component';
import { ModelListContainerComponent } from './model-list-container/model-list-container.component';
import { ModelListPageComponent } from './model-list-page/model-list-page.component';
import { ModelGetterService } from './model-getter.service';
import { ProductDetailPageComponent } from './product-detail-page/product-detail-page.component';
import { ProductDetailContainerComponent } from './product-detail-container/product-detail-container.component';
import { ProductGetGetterService } from './product-get-getter.service';
import { OrderProductCardComponent } from './order-product-card/order-product-card.component';
import { SetStatusComponent } from './set-status/set-status.component';
import { OrderExportContainerComponent } from './order-export-container/order-export-container.component';
import { OrderExportPageComponent } from './order-export-page/order-export-page.component';
import { OrderExportButtonComponent } from './order-export-button/order-export-button.component';
import { OrderExportSearchbarComponent } from './order-export-searchbar/order-export-searchbar.component';
import { OrderExportGetterService } from "app/order/order-export-getter.service";
import { OrderExportSenderService } from './order-export-sender.service';
import { ProductStatusSetterService } from './product-status-setter.service';
import { ProductDetailCardComponent } from './product-detail-card/product-detail-card.component';
import { ProductListGetterService } from './product-list-getter.service';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OrderRoutingModule,
    CoreModule
  ],
  declarations: [ProductCardComponent, ProductListComponent, ProductListPageComponent, ProductCardContainerComponent, ProductDetailComponent, ModelListComponent, ModelListContainerComponent, ModelListPageComponent,ProductDetailPageComponent, ProductDetailContainerComponent, OrderProductCardComponent, SetStatusComponent, OrderExportContainerComponent, OrderExportPageComponent, OrderExportButtonComponent, OrderExportSearchbarComponent, ProductDetailCardComponent],
  exports: [OrderRoutingModule],
  providers: [ProductGetterService, ModelGetterService, ProductGetGetterService, OrderExportGetterService, OrderExportSenderService,ProductStatusSetterService, ProductListGetterService]
})
export class OrderModule { }
