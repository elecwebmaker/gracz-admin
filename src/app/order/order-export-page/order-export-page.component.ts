import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderExportContainerComponent } from "app/order/order-export-container/order-export-container.component";
import { OrderExportSenderService } from "app/order/order-export-sender.service";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-order-export-page',
  templateUrl: './order-export-page.component.html',
  styleUrls: ['./order-export-page.component.scss']
})
export class OrderExportPageComponent implements OnInit {
  @ViewChild('container') order_container: OrderExportContainerComponent;
  constructor(public customer_export: OrderExportSenderService) { }
  states:CfIbreadState[] = [
    {
      name:"orderexport",
      text:"รายการส่งออก",
      url:"/product/export"
    } 
  ];

  ngOnInit() {
  }


  exportSuccess(isSuccess){
    if(isSuccess){
      this.order_container.refresh();
    }
  }
}
