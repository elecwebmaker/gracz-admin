import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Subject } from "rxjs/Rx";
import { Observable } from "rxjs/Observable";
import { SemanticModalComponent } from 'ng-semantic/ng-semantic';
import { IProductSetStatusModel, ProductStatusSetterService } from "app/order/product-status-setter.service";
import { ProductGetGetterService } from "app/order/product-get-getter.service";
import { NotifyService } from "craftutility/";

const APPROVE = 'APPROVE';
const REJECT = 'REJECT';

@Component({
    selector: 'app-set-status',
    templateUrl: './set-status.component.html',
    styleUrls: ['./set-status.component.scss']
})
export class SetStatusComponent implements OnInit {
    title: string;
    content: string;
    [x: string]: any;
    @Input() product_id: number;
    @Input() data: Array<{id: number, text: string}>;
    comfirm$: Subject<boolean> = new Subject<boolean>();
    click$: Subject<string> = new Subject<string>();
    @ViewChild('confirmModal') confirmModal: SemanticModalComponent;
    constructor(
        private productService: ProductGetGetterService,
        private productSetStatusService: ProductStatusSetterService,
        private notiservice: NotifyService
        ) { }

    ngOnInit() {
        Observable.zip(this.click$, this.comfirm$, function (click, confirm) {
            return [click, confirm];
        }).subscribe(([click, confirm]) => {
            if (confirm) {
                switch (click) {
                    case APPROVE:
                        this.setStatus(1);
                        break;
                    case REJECT:
                        this.setStatus(2);
                        break;
                    default:
                }
            }
        })
    }

    approve() {
        this.click$.next(APPROVE);
        this.title = 'อนุมัติสินค้า';
        this.content = 'กรุณากดปุ่มยืนยันเพื่ออนุมัติสินค้า';
        this.confirmModal.show();
    }

    disapprove() {
        this.click$.next(REJECT);
        this.title = 'ไม่อนุมัติสินค้า';
        this.content = 'กรุณากดปุ่มยืนยันเพื่อไม่อนุมัติสินค้า';
        this.confirmModal.show();
    }

    confirm() {
        this.comfirm$.next(true);
        this.confirmModal.hide();
    }

    cancel() {
        this.comfirm$.next(false);
        this.confirmModal.hide();
    }

    setStatus(status: number) {
        this.isCompleted = false;
        const body: IProductSetStatusModel = {
            product_id: this.product_id,
            status: status
        }
        return this.productSetStatusService.setStatus(body).subscribe(() => {
            this.notiservice.showSuccess();
            this.isCompleted = true;
            this.productService.refresh();
        });
    }
}
