import { Component, OnInit, forwardRef, Output } from '@angular/core';
import { ProductCardModel } from 'app/core/model/product-card.model';
import { CfPresentComponent } from "craftutility";
import { Subject } from "rxjs/Rx";
@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss'],
    providers:[
        {
            provide:CfPresentComponent,
            useExisting:forwardRef(()=>ProductListComponent)
        }
    ]
})
export class ProductListComponent extends CfPresentComponent<ProductCardModel> implements OnInit {
    @Output('onClick') onClick: Subject<ProductCardModel> = new Subject<ProductCardModel>();
    constructor() {
        super();
    }

    OnInit(){

    }


    click(item: ProductCardModel){
        this.onClick.next(item);
    }
}
