import { Component, OnInit, Input, EventEmitter,Output } from '@angular/core';
import { CfIContainerComp } from "app/shared/cf-comp-dispatcher/IcontainerComp";
import { ProductCardModel } from "app/core/model/product-card.model";
import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";
import { ProductGetterService } from "app/order/product-getter.service";
import { Router } from "@angular/router";
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-product-card-container',
  templateUrl: './product-card-container.component.html',
  styleUrls: ['./product-card-container.component.scss']
})
export class ProductCardContainerComponent implements OnInit, CfIContainerComp<Array<ProductCardModel>> {
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onNavChange({size,offset}){
    this.productgetter.filter.setLockFilter({
        size,
        offset
    });
  }
  subscription:Subscription;
  totaldata: number;
  resetPage:Subject<any>;


  data_dispatcher: CfDataDispatcher<ProductCardModel[]> = new CfDataDispatcher<ProductCardModel[]>();
  @Input() model_id: string;
  @Input() unit: string;
  @Output() getBreadCrumbData: EventEmitter<{model_code}> = new EventEmitter();
  constructor(
    private productgetter: ProductGetterService,
    private router: Router
  ) { }
  
  ngOnInit() {
    this.productgetter.filter.setLockFilter({
      model_id:this.model_id,
      unit: this.unit
    })
    this.subscription = this.productgetter.list().subscribe((data:any)=>{
      this.getBreadCrumbData.next({
        model_code:this.productgetter.model_code
      });
      this.data_dispatcher.setData(data.model);
      this.totaldata = data.total;
    })
    this.resetPage = this.productgetter.resetPage;
    
  }

  selectProduct(product: ProductCardModel){
    this.router.navigate([`/model/product/detail/${product.oid}/${product.pid}`])
  }
}

