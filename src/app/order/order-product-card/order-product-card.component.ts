import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-product-card',
  templateUrl: './order-product-card.component.html',
  styleUrls: ['./order-product-card.component.scss']
})
export class OrderProductCardComponent implements OnInit {
  @Input() data: any
  constructor() { }

  ngOnInit() {
  }

}
