import { Component, OnInit } from '@angular/core';
import { CFTableControl, CFRow } from "craft-table/";
import { IOrderExportModel } from "app/core/model/order-export.model";
import { OrderExportGetterService } from "app/order/order-export-getter.service";
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-order-export-container',
    templateUrl: './order-export-container.component.html',
    styleUrls: ['./order-export-container.component.scss']
})
export class OrderExportContainerComponent implements OnInit {
    public chk_data = [];
    subscription:Subscription;
    ngOnDestroy(){
        this.subscription.unsubscribe();
    }
    control: CFTableControl = new CFTableControl({
        header: [
        {
            id: '@checkbox',
            label: 'ทั้งหมด'
        },
        {
            id: 'preorder_id',
            label: 'รหัส PSO'
        }, {
            id: 'customer_code',
            label: 'ID ลูกค้า'
        }, {
            id: 'customer_name',
            label: 'ชื่อลูกค้า'
        }, {
            id: 'shipment_date',
            label: 'วันที่จัดส่ง'
        }, {
            id: 'total_price',
            label: 'ราคารวมภาษี'
        }],
        navigating: true
    });
    currentPage:number = 1;
    currentSize:number = 10;
    observableList;
    datatotal: number = 0;
    constructor(private router: Router, private orderExportGetterService: OrderExportGetterService) {
        this.observableList =  this.orderExportGetterService.list().map((res:any)=>{
            this.control.setDataTotal(res.total);
            this.datatotal = res.total;
            return res.model;
        });
    }

    ngOnInit() {
        // this.control.setData(this.data);
        this.control.setGetData((offset: number, size: number) => {
            this.orderExportGetterService.filter.setLockFilter({
                size,
                offset
            });
            return this.observableList;
        });
        this.subscription = this.orderExportGetterService.resetPage.subscribe(()=>{
            this.currentPage = 1;
        });
    }
    refresh(){
        this.control.refresh();
        this.chk_data = [];
    }
    onCheckbox(item: Array<CFRow>) {
        this.chk_data = item.map((row) => {
            return row.get('order_id').getValue();
        });
    }

    viewDetail(cfrow: {row:CFRow}){
        const order_id = cfrow.row.get('order_id').getValue();
        const product_id = cfrow.row.get('product_id').getValue();
        this.router.navigate([`/model/product/detail/${order_id}/${product_id}`])
    }
}
