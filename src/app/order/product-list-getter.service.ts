import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from 'craftutility';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { UrlProviderService, PRODUCT_DETAIL_LIST } from 'app/shared/url-provider.service';

@Injectable()
export class ProductListGetterService extends CfGetterService<any>{
  filter: Filterservice<any> = new Filterservice();
  listprovider(filter: any): Observable<any> {
    const url = this.urlprovider.getUrl(PRODUCT_DETAIL_LIST);
    return this.http
    .post(url, {
      order_id: filter.order_id,
      product_id: filter.product_id
    }).map((res: any) => {
      const model = res.product_list.map((product)=>{
        return {
          product_code:product.product_code,
          name:product.name,
          quantity:product.quantity,
          unit:product.unit_text,
          price:product.price_text,
          status_text:product.status_text,
          product_id:product.product_id
        }
      });
      
      return {
        model,
        total:model.length
      }
    });
  }
  constructor(
    private http: Http,
    private urlprovider: UrlProviderService
  ) {
    super();
  }

}
