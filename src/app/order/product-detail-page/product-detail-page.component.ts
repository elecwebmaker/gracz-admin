import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-product-detail-page',
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.scss']
})
export class ProductDetailPageComponent implements OnInit {
  product_id: string;
  order_id: string;
  mapParams:Object;
  constructor(
    private activatedroute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.product_id = this.activatedroute.snapshot.params['product_id'];
    this.order_id = this.activatedroute.snapshot.params['order_id'];

  }

  setBreadCrumbData(data:{model_code:string,unit:string,model_id:string,pcode: string}){
    this.mapParams = {
      productdetail:{
        product_id:data.pcode
      },
      productlist:{
        unit_type:data.unit,
        model_code:data.model_code,
        model_id:data.model_id
      }
    }
  }
}
