import { TestBed, inject } from '@angular/core/testing';

import { ProductListGetterService } from './product-list-getter.service';

describe('ProductListGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductListGetterService]
    });
  });

  it('should ...', inject([ProductListGetterService], (service: ProductListGetterService) => {
    expect(service).toBeTruthy();
  }));
});
