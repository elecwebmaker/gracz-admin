import { Component, OnInit, forwardRef, Input, OnChanges, SimpleChanges } from "@angular/core";
import { CFTableControl, CFRow } from "craft-table/";
import { CfPresentComponent } from "craftutility";
import { IOrderProductModel ,OrderProductModel } from "app/core/model/order-product.model";
import { Router } from "@angular/router/";
import { ProductListGetterService } from "app/order/product-list-getter.service";

@Component({
  selector: "app-product-detail",
  templateUrl: "./product-detail.component.html",
  styleUrls: ["./product-detail.component.scss"],
  providers: [
    {
      provide: CfPresentComponent,
      useExisting: forwardRef(() => ProductDetailComponent)
    }
  ]
})
export class ProductDetailComponent extends CfPresentComponent<IOrderProductModel> implements OnChanges {
  ngOnChanges(changes: SimpleChanges): void {

    if(changes && this.p_id && this.o_id){
     
      this.productlistgetter.filter.setLockFilter({
        product_id:this.p_id,
        order_id:this.o_id
      })
    }
  }
  @Input() p_id: number;
  @Input() o_id: number;
  tableControl: CFTableControl = new CFTableControl({
    header: [
      {
        id: 'product_code',
        label: 'สินค้า'
      }, {
        id: 'name',
        label: 'ชื่อสินค้า'
      }, {
        id: 'quantity',
        label: 'จำนวน'
      }, {
        id: 'unit',
        label: 'หน่วย'
      }, {
        id: 'price',
        label: 'ราคา'
      }, {
        id: 'status_text',
        label: 'สถานะสั่งซื้อ'
      }
    ],
    navigating: true
  });
  constructor(
    private router: Router,
    private productlistgetter: ProductListGetterService
  ) {
    super();
  }
  currentPage:number = 1;
  currentSize:number = 10;
  observableList;
  datatotal: number = 0;
  OnInit() {
    this.productlistgetter.filter.setLockFilter({
      product_id:this.p_id,
      order_id:this.o_id
    })
    this.observableList = this.productlistgetter.list().map((res:any)=>{
      this.tableControl.setDataTotal(res.total);
      this.datatotal = res.total;
      return res.model;
    });

    this.tableControl.setGetData((offset:number,size:number)=>{
      this.productlistgetter.filter.setLockFilter({
          size,
          offset
      });
      return this.observableList;
    });
  }

  goto(cfrow: {row: CFRow}) {
    console.log(cfrow);
    const p_id = cfrow.row.get('product_id').getValue();
    this.router.navigate([`/model/product/detail/${this.o_id}/${p_id}`]);
    // this.router.navigate(['/product/detail', this.o_id, p_id]);
  }
}
