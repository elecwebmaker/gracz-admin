import { Injectable } from '@angular/core';
import { CfGetterService, Filterservice } from "craftutility";
import { ProductModelModel } from "app/core/model/product-model.model";
import { MODEL_LIST, UrlProviderService } from "app/shared/url-provider.service";
import { Observable } from "rxjs/Rx";
import { Http } from "@angular/http";
import { Subject } from 'rxjs/Rx';
export interface IModelFilter{
  key?:string;
  unit?: string;
  size?: number;
  offset?: number;
}

@Injectable()
export class ModelGetterService extends CfGetterService<ProductModelModel>{

  filter: Filterservice<IModelFilter> = new Filterservice<IModelFilter>();
  resetPage = new Subject();
  listprovider(filter:IModelFilter): Observable<any> {
    const url = this.urlprovider.getUrl(MODEL_LIST);
    return this.http.post(url,filter).map((res:any)=>{
      const result = res.model;
      return {
        model:result.map((item)=>{
          return new ProductModelModel({
              model_id:item.model_id,
              model_name:item.model_text,
              quantity:item.remain,
              unit:item.unit_text,
              total_order:item.order_total
          });
        }),
        total:res.total
      };
      
      
    });
  }

  constructor(private http: Http,private urlprovider: UrlProviderService) {
    super();
  }

}
