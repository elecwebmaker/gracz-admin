import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { ICustomerExportFilter } from "app/customer/customer-export-getter.service";
import { KeywordListService } from "app/core/keyword-list.service";
import { OrderExportGetterService, IOrderExportFilter } from "app/order/order-export-getter.service";

@Component({
  selector: 'app-order-export-searchbar',
  templateUrl: './order-export-searchbar.component.html',
  styleUrls: ['./order-export-searchbar.component.scss']
})
export class OrderExportSearchbarComponent implements OnInit {
  public filter = new FilterBar<IOrderExportFilter>(["key"]);
  constructor(private modelgetter: OrderExportGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
    this.filter.filterChanges().subscribe((res:any) => {
        this.modelgetter.filter.setLockFilter({
            key:res.key,
        })
    });
  }

  searchChange(val) {
      this.filter.setFilter('key', val);
  }
}
