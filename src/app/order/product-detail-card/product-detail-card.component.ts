import { Component, Input, OnChanges } from "@angular/core";
import { ProductCardDetailModel } from 'app/core/model/product-card-detail.model';

@Component({
  selector: 'app-product-detail-card',
  templateUrl: './product-detail-card.component.html',
  styleUrls: ['./product-detail-card.component.scss']
})
export class ProductDetailCardComponent implements OnChanges {
  @Input() data: ProductCardDetailModel | undefined;
  statusColor: {};

  constructor() { }

  ngOnChanges() {
    console.log(this.data);
    if (this.data) {
      this.statusColor = {
        positive: this.data.status_text === 'อนุมัติ',
        negative: this.data.status_text === 'ไม่อนุมัติ',
        warning: this.data.status_text === 'รออนุมัติ'
      };
    }
  }

}
