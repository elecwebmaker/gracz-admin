import { Component, OnInit, Input, EventEmitter, Output,OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation-eve',
  templateUrl: './navigation-eve.component.html',
  styleUrls: ['./navigation-eve.component.scss']
})
export class NavigationEveComponent implements OnInit,OnDestroy {
  subscription:Subscription;
  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
    
  }
  @Input('currentpage') intialpage: number = 1;
  @Input('currentsize') intialsize: number = 10;
  @Output('onChange') onChange = new EventEmitter<{size,offset}>();
  @Input('resetPage') resetPage:Subject<any>;
  @Input('sizeAvailable') sizeAvailable :Array<number> = [10,20,30,40,50];
  @Input('total') total: number = 50;
  size:number;
  offset:number;
  constructor() { }
  pageChange(page){
    const offset = (page - 1) * this.size;  
    if(this.offset !== offset && offset >= 0){
      this.offset = offset;
      this.navigateChange();
    }

  }

  sizeChange(size){
    if(this.size !== size){
      this.size = size;
      this.navigateChange();
    }
    
  }

  navigateChange(){
    console.log(this.size,this.offset,'pl');
    this.onChange.emit({
      size:this.size,
      offset: this.offset
    });
  }
  ngOnInit() {
      this.size = this.intialsize;
      this.offset = (this.intialpage - 1) * this.size;
      if(this.resetPage){
        this.subscription = this.resetPage.subscribe(()=>{
          this.intialpage = 1;
        });
      }
  }

  

}
