import { Component, OnInit, Input, ViewChildren, QueryList, Output, EventEmitter } from '@angular/core';
import { IUserListModel, UserListModel } from "app/core/model/user-list.model";
import { EditBoxComponent } from "app/core/edit-box/edit-box.component";
import { UrlProviderService, UPLOAD_FILE } from "app/shared/url-provider.service";

@Component({
    selector: 'app-account-detail',
    templateUrl: './account-detail.component.html',
    styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {
    @ViewChildren(EditBoxComponent) editBox: QueryList<EditBoxComponent>;
    @Input() data: UserListModel;
    @Output() edit: EventEmitter<any> = new EventEmitter();
    @Output('uploadSuccess') uploadSuccessEvt: EventEmitter<any> = new EventEmitter();
    urlupload: string;
    constructor(private urlprovider: UrlProviderService) {

    }

    ngOnInit() {
        this.urlupload = this.urlprovider.getUrl(UPLOAD_FILE);
        if(!this.data){
            this.data = <UserListModel>{};
        }
    }

    onEdit(event, refComp) {
        this.editBox.forEach(element => {
            element.deactive();
        });
        refComp.active();
    }

    uploadSuccess(res:any){
        this.uploadSuccessEvt.next(res.database);
    }

    uploadError(){

    }

    onSavePhone(value, refComp) {
        if (value !== this.data.phone) {
            this.data.phone = value;
            this.edit.emit({phone_code: value.code, phone_number: value.number});
        }
        refComp.deactive();
    }

    onSaveLine(value, refComp) {
        if (value !== this.data.line_id) {
            this.data.line_id = value;
            this.edit.emit({line_id: value});
        }
        refComp.deactive();
    }

}
