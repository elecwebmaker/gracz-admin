import { Component, OnInit, ContentChild } from '@angular/core';
import { MaskAction } from 'app/shared/cf-mask-action/mask-action';
import { MaskPresentComp } from 'app/shared/cf-mask-action/mask-present-comp';
import { Router } from '@angular/router';

@Component({
  selector: 'app-router-tab',
  templateUrl: './router-tab.component.html',
  styleUrls: ['./router-tab.component.scss']
})
export class RouterTabComponent extends MaskAction {
    @ContentChild(MaskPresentComp) presentComp: MaskPresentComp;

    constructor(private router: Router) {
        super();
    }

    action(value: any): void {
        this.router.navigate([value]);
    }
}
