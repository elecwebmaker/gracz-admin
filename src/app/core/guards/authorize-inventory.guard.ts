import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CfAuthorizeJWTGuard } from "app/cfusersecure/cf-authorize-guard/cf-authorize-jwt-guard";

@Injectable()
export class AuthorizeInventoryGuard implements CanActivate {
    constructor(
        private cfJwtSecure: CfAuthorizeJWTGuard
    ) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.cfJwtSecure.isAuthorize(3);
    }
}
