import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizeUserGuard } from './authorize-user.guard';

describe('AuthorizeUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizeUserGuard]
    });
  });

  it('should ...', inject([AuthorizeUserGuard], (guard: AuthorizeUserGuard) => {
    expect(guard).toBeTruthy();
  }));
});
