import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms/";
import { validateConfirmPassword } from "app/core/cf-validate-confirm-password";

export interface IResetPass {
    old_password: string;
    new_password: string;
    confirm_password: string;
}

@Component({
    selector: 'app-reset-pass',
    templateUrl: './reset-pass.component.html',
    styleUrls: ['./reset-pass.component.scss']
})

export class ResetPassComponent implements OnInit {
    public formg: FormGroup;
    public errorMsg: string;
    @Output() setPass: EventEmitter<IResetPass> = new EventEmitter<IResetPass>();
    @Input() loading: boolean;
    constructor( private _fb: FormBuilder) { }

    ngOnInit() {
        this.formg = this._fb.group({
            curPass: ['', Validators.required],
            newPass: [''],
            renewPass: ['']
        });
        this.formg.get('newPass').setValidators([Validators.required, validateConfirmPassword(<FormControl>this.formg.get('renewPass'), true)]);
        this.formg.get('renewPass').setValidators([Validators.required, validateConfirmPassword(<FormControl>this.formg.get('newPass'))]);
    }

    submit() {
        if (this.formg.valid) {
            this.setPass.emit({
                old_password: this.formg.get('curPass').value,
                new_password: this.formg.get('newPass').value,
                confirm_password: this.formg.get('renewPass').value
            })
        }
    }

    errorPass(msg: string) {
        this.errorMsg = msg;
    }
}
