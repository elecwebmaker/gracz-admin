import { Injectable } from '@angular/core';

import { Http } from "@angular/http";
import { CfUserHandlerNormalService } from "app/cfusersecure/cf-user-handler/cf-user-handler-normal.service";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { API_URL } from "config";

@Injectable()
export class TestUserService {

  constructor(private cfuserhandlernormalservice:CfUserHandlerNormalService,private cfuserjwtservice: CfUserHandlerJWTService,private http: Http) { 

  }

  login(isSave:boolean){
    //username:string, password: string
    const url = API_URL + '/user/login';
    this.http.post(url,{username:'qwe',password:'qwe'}).subscribe((res:any)=>{
      this.cfuserhandlernormalservice.login(res.token,{
        user_id:"testid",
        nameer:"Napat"
      });
      if(isSave){
        this.cfuserhandlernormalservice.saveToLocalStorage();
      }
    });
 
  }

  loginJwt(isSave){
    const url = API_URL + '/user/login';
    this.http.post(url,{username:'admin',password:'1234'}).subscribe((res:any)=>{
      this.cfuserjwtservice.login(res.token,[1,2,3,4,5,6]);
      if(isSave){
        this.cfuserjwtservice.saveToLocalStorage();
      }
    });
  }

  getData(){
    return {
      user_id:this.cfuserhandlernormalservice.getUserData('user_id'),
      name: this.cfuserhandlernormalservice.getUserData('nameer')
    }
  }

  getDataJWT(){
    return {
      name: this.cfuserjwtservice.getUserData('name'),
      email: this.cfuserjwtservice.getUserData('email')
    }
  }

  isAuthen(){
    return this.cfuserhandlernormalservice.isAuthen();
  }
  isAuthenJWT(){
    return this.cfuserjwtservice.isAuthen();
  }
  logout(){
    this.cfuserhandlernormalservice.logout();
    this.cfuserjwtservice.logout();
  }

  getAuthorizes(){
    return this.cfuserhandlernormalservice.getAuthorize();
  }

  getJwtAuthorizes(){
    return this.cfuserjwtservice.getAuthorize();
  }

  isAuthorize(num){
      return  this.cfuserjwtservice.isAuthorize(num);
  }
}
