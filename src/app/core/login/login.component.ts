import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms/";

declare var $: any;
export interface LoginUser {
    username: string;
    password: string;
    savePass: boolean;
};

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    @Output() onLogin: EventEmitter<LoginUser> = new EventEmitter<LoginUser>();
    formG: FormGroup;
    ispasswrong: boolean = false;
    hasSubmit: boolean = false;
    constructor(private _fb: FormBuilder) {

    }

    ngOnInit() {
        this.formG = this._fb.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
            isSave: [false]
        })
    }

    ngAfterViewInit() {
        $('.ui.checkbox').checkbox();
    }

    login() {
        if (this.formG.valid) {
            this.hasSubmit = true;
            const formValue = this.formG.value;
            this.onLogin.emit({
                username: formValue.username,
                password: formValue.password,
                savePass: formValue.isSave
            });
        }
    }

    setPassWrong(ispasswrong) {
        this.ispasswrong = ispasswrong;
    }
}
