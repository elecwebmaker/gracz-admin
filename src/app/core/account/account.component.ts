import { Component, Input, EventEmitter, Output, AfterViewInit } from '@angular/core';

declare var $;

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss']
})
export class AccountComponent implements AfterViewInit {
    @Input() name: string;
    @Input() img: string;
    @Output() myAccount: EventEmitter<any> = new EventEmitter<any>();
    @Output() logout: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }

    ngAfterViewInit() {
        $('.ui.dropdown').dropdown({
            action: 'hide'
        });
    }

    myAcc() {
        this.myAccount.emit();
    }

    signOut() {
        this.logout.emit();
    }
}
