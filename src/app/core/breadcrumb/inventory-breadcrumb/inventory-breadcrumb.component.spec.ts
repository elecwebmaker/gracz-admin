import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryBreadcrumbComponent } from './inventory-breadcrumb.component';

describe('InventoryBreadcrumbComponent', () => {
  let component: InventoryBreadcrumbComponent;
  let fixture: ComponentFixture<InventoryBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
