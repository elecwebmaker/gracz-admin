import { Component, OnInit, Input } from '@angular/core';
import { BreadCrumbBase } from "app/core/breadcrumb/breadcrumb-base";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-inventory-breadcrumb',
  templateUrl: './../breadcrumb.component.html',
  styleUrls: ['./inventory-breadcrumb.component.scss']
})
export class InventoryBreadcrumbComponent extends BreadCrumbBase implements OnInit {  
  @Input() states:CfIbreadState[] = [
    {
      name:"inventorylist",
      text:"คลังสินค้า",
      url:"/inventory/list"
    }
  ];
  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
