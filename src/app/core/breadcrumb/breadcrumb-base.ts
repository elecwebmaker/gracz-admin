import { Input } from "@angular/core";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

export abstract class BreadCrumbBase {
    abstract states:Array<CfIbreadState>;
    @Input() current:string;
    @Input() mapParams:Object;
}