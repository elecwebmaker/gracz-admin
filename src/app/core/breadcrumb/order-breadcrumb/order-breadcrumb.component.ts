import { Component, OnInit, Input } from '@angular/core';
import { BreadCrumbBase } from "app/core/breadcrumb/breadcrumb-base";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-order-breadcrumb',
  templateUrl: './../breadcrumb.component.html',
  styleUrls: ['./order-breadcrumb.component.scss']
})
export class OrderBreadcrumbComponent extends BreadCrumbBase implements OnInit {
  @Input() states:CfIbreadState[] = [
        {
      name:"modellist",
      text:"สรุปข้อมูลการสั่งสินค้า",
      url:"/model/list"
    },
    {
      name:"productlist",
      text:"รายการสินค้า โมเดลที่ {{model_code}} และ หน่วย {{unit_type}}",
      url:"/model/product/{{model_id}}/{{unit_type}}/list"
    },
    {
      name:"productdetail",
      text:"รายละเอียดสินค้ารหัส {{product_id}}"
    }
  ];
  constructor() { 
    super();
  }

  ngOnInit() {
  }

}
