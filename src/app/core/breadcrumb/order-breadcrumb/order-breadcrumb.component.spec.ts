import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderBreadcrumbComponent } from './order-breadcrumb.component';

describe('OrderBreadcrumbComponent', () => {
  let component: OrderBreadcrumbComponent;
  let fixture: ComponentFixture<OrderBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
