import { Component, OnInit, Input } from '@angular/core';
import { BreadCrumbBase } from "app/core/breadcrumb/breadcrumb-base";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

@Component({
  selector: 'app-customer-breadcrumb',
  templateUrl: './../breadcrumb.component.html',
  styleUrls: ['./customer-breadcrumb.component.scss']
})
export class CustomerBreadcrumbComponent extends BreadCrumbBase implements OnInit {
  @Input() states:CfIbreadState[] = [
    {
      name:"customerlist",
      text:"รายการลูกค้า",
      url:"/customer/list"
    },
    {
      name:"customerdetail",
      text:"รายละเอียดลูกค้ารหัส {{id}}",
      url:"/customer/detail/{{id}}"
    }  
  ];
  
  constructor() {
    super();
   }

  ngOnInit() {
  }

}
