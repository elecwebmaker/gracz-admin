import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerBreadcrumbComponent } from './customer-breadcrumb.component';

describe('CustomerBreadcrumbComponent', () => {
  let component: CustomerBreadcrumbComponent;
  let fixture: ComponentFixture<CustomerBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
