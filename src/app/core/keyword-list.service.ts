import { Injectable } from '@angular/core';
import { UrlProviderService, LIST_MODEL_UNIT, LIST_PRODUCT_STATUS, LIST_INVENTORY_UNIT, LIST_CUSTOMER_STATUS, LIST_JOB_TYPE } from "app/shared/url-provider.service";
import { Http } from "@angular/http";
import { KeywordModel, IkeywordModel } from "app/core/model/keyword.model";
import { Observable } from "rxjs/Rx";

@Injectable()
export class KeywordListService {

    constructor(
        private urlprovider: UrlProviderService,
        private http: Http
    ) {

    }

    listModelUnit(): Observable<IkeywordModel[]> {
        const url = this.urlprovider.getUrl(LIST_MODEL_UNIT);
        return this.http.post(url, {}).map((res: any) => {
            return res.model.map((item) => {
                return new KeywordModel({
                    id: item.id,
                    text: item.text
                })
            });
        });
    }

    listProductStatus(): Observable<IkeywordModel[]> {
        const url = this.urlprovider.getUrl(LIST_PRODUCT_STATUS);
        return this.http.post(url, {}).map((res: any) => {
            return res.model.map((item) => {
                return new KeywordModel({
                    id: item.id,
                    text: item.text
                })
            });
        });
    }

    listInventoryUnit(): Observable<IkeywordModel[]> {
        const url = this.urlprovider.getUrl(LIST_INVENTORY_UNIT);
        return this.http.post(url, {}).map((res: any) => {
            return res.model.map((item) => {
                return new KeywordModel({
                    id: item.id,
                    text: item.text
                })
            });
        });
    }

    listCustomerStatus(): Observable<IkeywordModel[]> {
        const url = this.urlprovider.getUrl(LIST_CUSTOMER_STATUS);
        return this.http.post(url, {}).map((res: any) => {
            return res.model.map((item) => {
                return new KeywordModel({
                    id: item.id,
                    text: item.text
                })
            });
        });
    }

    listJobType(): Observable<IkeywordModel[]> {
        const url = this.urlprovider.getUrl(LIST_JOB_TYPE);
        return this.http.post(url, {}).map((res: any) => {
            return res.model.map((item) => {
                return new KeywordModel({
                    id: item.id,
                    text: item.text
                });
            });
        });
    }
}
