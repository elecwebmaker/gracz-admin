import { Component, OnInit } from '@angular/core';
import { UserAuthenService } from 'app/core/user-authen.service';
import { Router } from '@angular/router/';

@Component({
    selector: 'app-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss']
})
export class TopBarComponent implements OnInit {
    img = this.userAuthenService.getUserData().picture_src;
    name = this.userAuthenService.getUserData().name;
    constructor(private userAuthenService: UserAuthenService, private router: Router) { }

    ngOnInit() {
    }

    logout() {
        this.userAuthenService.logout().subscribe(() => {
            this.router.navigate(['/login']);
        });
    }

    account() {
        this.router.navigate(['/profile']);
    }
}
