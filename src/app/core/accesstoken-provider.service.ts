import { Injectable } from '@angular/core';
import { CfUserHandlerJWTService } from 'app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service';
import { TokenProvider } from 'craftutility/';

@Injectable()
export class AccesstokenProviderService implements TokenProvider {

    constructor(private cfuserjwtservice: CfUserHandlerJWTService) { }

    getToken(): string {
        return this.cfuserjwtservice.getAccesstoken();
    }
}
