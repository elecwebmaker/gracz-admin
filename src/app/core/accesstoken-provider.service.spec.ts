import { TestBed, inject } from '@angular/core/testing';

import { AccesstokenProviderService } from './accesstoken-provider.service';

describe('AccesstokenProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccesstokenProviderService]
    });
  });

  it('should ...', inject([AccesstokenProviderService], (service: AccesstokenProviderService) => {
    expect(service).toBeTruthy();
  }));
});
