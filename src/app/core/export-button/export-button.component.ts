import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ConfirmModalComponent } from "app/shared/confirm-modal/confirm-modal.component";
import { CustomerExportSenderService } from "app/customer/customer-export-sender.service";
import { NotifyService } from "craftutility";

@Component({
    selector: 'app-export-button',
    templateUrl: './export-button.component.html',
    styleUrls: ['./export-button.component.scss']
})
export class ExportButtonComponent implements OnInit {
    @Input() toSubmit: Array<string> = [];
    @ViewChild(ConfirmModalComponent) confirmModal: ConfirmModalComponent;
    @Output() onExportSuccess: EventEmitter<boolean> = new EventEmitter<boolean>();
    isComplete: boolean = true;
    @Input() exportservice;
    constructor(
        private notify: NotifyService
    ) { }

    ngOnInit() {
       
        this.confirmModal.comfirm$.subscribe((isConfirm)=>{
             this.isComplete = false;
            if(isConfirm){
               this.exportservice.export(this.toSubmit).subscribe(()=>{
                this.notify.showSuccess();
                this.onExportSuccess.emit(true);
                this.isComplete = true;
               });
            }
        });
    }

    onSubmit() {
        if(!this.toSubmit || this.toSubmit.length <= 0){
            this.notify.showError('ต้องเลือกอย่างน้อย 1 รายการ');
        }else{
            this.confirmModal.show();
        }
        
        
    }
}
