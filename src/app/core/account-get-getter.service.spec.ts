import { TestBed, inject } from '@angular/core/testing';

import { AccountGetGetterService } from './account-get-getter.service';

describe('AccountGetGetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountGetGetterService]
    });
  });

  it('should ...', inject([AccountGetGetterService], (service: AccountGetGetterService) => {
    expect(service).toBeTruthy();
  }));
});
