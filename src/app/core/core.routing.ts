import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginContainerComponent } from 'app/core/login-container/login-container.component';
import { TestLoginViewComponent } from "app/core/testloginview/testloginview.component";
import { TestloginComponent } from "app/core/testlogin/testlogin.component";
import { AccountDetailPageComponent } from "app/core/account-detail-page/account-detail-page.component";
import { ResetPassPageComponent } from "app/core/reset-pass-page/reset-pass-page.component";
import { NotfoundComponent } from "app/core/notfound/notfound.component";



const routes: Routes = [
    {
        path: 'login',
        component: LoginContainerComponent
    },
    {
        path: 'testlogin',
        component: TestloginComponent
    },
    {
        path: 'test',
        component: TestLoginViewComponent
    }, {
        path: 'profile',
        component: AccountDetailPageComponent
    }, {
        path: 'resetpass',
        component: ResetPassPageComponent
    },{
        path:'',
        redirectTo:"login",
        pathMatch: 'full'

    },
    {path: '404', component: NotfoundComponent},
    {path: '*', redirectTo: '/404'}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
})
export class CoreRoutingModule { }
