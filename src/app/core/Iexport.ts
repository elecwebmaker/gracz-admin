import { Observable } from "rxjs/Rx";

export interface Iexport {
    export(ids:Array<string>):Observable<any>;
}