import { Component, OnInit } from '@angular/core';
import { TestUserService } from "app/core/testuser.service";
@Component({
  selector: 'app-testlogin',
  templateUrl: './testlogin.component.html',
  styleUrls: ['./testlogin.component.scss']
})
export class TestloginComponent{

  constructor(private userService: TestUserService) { }

   testlogin(isSave){
    this.userService.login(isSave);
  }

  logout(){
    this.userService.logout();
  }

  loginJwt(isSave){
    this.userService.loginJwt(isSave);
  }
}
