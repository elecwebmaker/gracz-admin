import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBoxPhoneComponent } from './edit-box-phone.component';

describe('EditBoxPhoneComponent', () => {
  let component: EditBoxPhoneComponent;
  let fixture: ComponentFixture<EditBoxPhoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBoxPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBoxPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
