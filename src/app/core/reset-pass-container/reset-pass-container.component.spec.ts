import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPassContainerComponent } from './reset-pass-container.component';

describe('ResetPassContainerComponent', () => {
  let component: ResetPassContainerComponent;
  let fixture: ComponentFixture<ResetPassContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetPassContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPassContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
