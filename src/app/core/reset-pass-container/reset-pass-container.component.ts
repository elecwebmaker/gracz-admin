import { Component, OnInit, ViewChild } from '@angular/core';
import { IResetPass, ResetPassComponent } from "app/core/reset-pass/reset-pass.component";
import { AccountSetterService } from "app/core/account-setter.service";
import { Router } from "@angular/router/";
import { NotifyService } from "craftutility/";
import { UserAuthenService } from 'app/core/user-authen.service';

@Component({
    selector: 'app-reset-pass-container',
    templateUrl: './reset-pass-container.component.html',
    styleUrls: ['./reset-pass-container.component.scss']
})
export class ResetPassContainerComponent implements OnInit {
    @ViewChild(ResetPassComponent) resetPassComp: ResetPassComponent;
    public loading: boolean;
    constructor(
        private accountSetter: AccountSetterService,
        private router: Router,
        private notify: NotifyService,
        private userAuthen: UserAuthenService
    ) { }

    ngOnInit() {
        this.loading = false;
    }

    setPass(item: IResetPass) {
        this.loading = true;
        this.accountSetter.resetPassword(item).subscribe(() => {
            this.notify.showSuccess('เปลี่ยนรหัสผ่านสำเร็จ');
            
            this.userAuthen.logout().subscribe(()=>{
                this.router.navigate(['/login']);
            });
        }, (error) => {
            const err_json = error.json();
            this.resetPassComp.errorPass(err_json.message);
            this.loading = false;
        });
    }

}
