import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MaskPresentComp } from 'app/shared/cf-mask-action/mask-present-comp';

@Component({
    selector: 'app-tab',
    templateUrl: './tab.component.html',
    styleUrls: ['./tab.component.scss'],
    providers:[{
        provide: MaskPresentComp,
        useExisting: forwardRef(() => TabComponent)
    }]
})
export class TabComponent extends MaskPresentComp {
    @Input() data: Array<{value:any,text:string, permission: number}> = [];
    @Input() current: string;
    tabValue = new FormControl();

    constructor() {
        super();
    }

    ngOnInit() {
        this.tabValue.valueChanges.subscribe((item) => {
            this.maskValueChanges.emit(item);
        });
    }

}
