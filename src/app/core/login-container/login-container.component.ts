import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginUser, LoginComponent } from 'app/core/login/login.component';
import { CfUserHandlerJWTService } from 'app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service';
import { UserAuthenService } from 'app/core/user-authen.service';
import { Router } from '@angular/router/';

@Component({
    selector: 'app-login-container',
    templateUrl: './login-container.component.html',
    styleUrls: ['./login-container.component.scss']
})
export class LoginContainerComponent implements OnInit {
    @ViewChild(LoginComponent) loginComp: LoginComponent;

    constructor(
        private cfUserAuthen: CfUserHandlerJWTService,
        private userAuthenService: UserAuthenService,
        private router: Router
    ) { }

    ngOnInit() {
        if(this.cfUserAuthen.isAuthen()){
            this.router.navigate(['/profile'])
        }
    }

    login(data: LoginUser) {
        this.userAuthenService.login(data).subscribe(() => {
            this.router.navigate(['/customer/list']);
            this.loginComp.setPassWrong(false);
        }, () => {
            this.loginComp.setPassWrong(true);
        });
    }

}
