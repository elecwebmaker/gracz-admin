import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { ProductGetterService, ProductListFilter } from "app/order/product-getter.service";
import { KeywordListService } from "app/core/keyword-list.service";

@Component({
  selector: 'app-productlist-searchbar',
  templateUrl: './productlist-searchbar.component.html',
  styleUrls: ['./productlist-searchbar.component.scss']
})
export class ProductlistSearchbarComponent implements OnInit {
  public filter = new FilterBar<ProductListFilter>(["status","key"]);
  constructor(private productgetter: ProductGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
        this.filter.filterChanges().subscribe((res) => {
            this.productgetter.filter.setLockFilter({
                key:res.key,
                status:res.status
            });
            this.productgetter.resetPage.next();

        });

        this.filter.setStatusList(this.keywordlist.listProductStatus());
  }


  searchChange(val) {
      this.filter.setFilter('key', val);
  }

  statusChange(val) {
      this.filter.setFilter('status', val);
  }
}
