import { Component, OnInit } from '@angular/core';
import { FilterBar } from "craftutility";
import { ModelGetterService, IModelFilter } from "app/order/model-getter.service";
import { KeywordListService } from "app/core/keyword-list.service";
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'app-modellist-searchbar',
  templateUrl: './modellist-searchbar.component.html',
  styleUrls: ['./modellist-searchbar.component.scss']
})
export class ModellistSearchbarComponent implements OnInit {
  public filter = new FilterBar<IModelFilter>(["key","unit"]);
  constructor(private modelgetter: ModelGetterService,private keywordlist: KeywordListService) { }

  ngOnInit() {
    this.filter.filterChanges().subscribe((res:any) => {
        this.modelgetter.filter.setLockFilter({
            key:res.key,
            unit:res.unit
        });
        this.modelgetter.resetPage.next();
    });
    this.filter.setStatusList(
        <Observable<{id,text}[]>>this.keywordlist.listModelUnit()
    );
    
  }

  searchChange(val) {
      this.filter.setFilter('key', val);
  }

  statusChange(val) {
      this.filter.setFilter('unit', val);
  }
}
