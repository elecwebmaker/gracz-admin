export interface IFilterKey {
    status: string;
    key: string;
}