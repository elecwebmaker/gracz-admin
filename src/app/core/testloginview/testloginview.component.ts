import { Component, OnInit } from '@angular/core';
import { TestUserService } from "app/core/testuser.service";

@Component({
  selector: 'app-testcomponent',
  templateUrl: './testloginview.component.html',
  styleUrls: ['./testloginview.component.scss']
})
export class TestLoginViewComponent implements OnInit {
  data: any;
  datajwt: any;
  isAuthen : boolean;
  isAuthenJWT : boolean;
  authorizes: any;
  jwtauthorizes: any;
  constructor(private userService: TestUserService) { }

  ngOnInit() {
    // this.isAuthen = this.userService.isAuthen(); //need to swap
    this.isAuthenJWT = this.userService.isAuthenJWT();
    this.data = this.userService.getData();
    this.datajwt = this.userService.getDataJWT();
    this.authorizes = this.userService.getAuthorizes();
    this.jwtauthorizes = this.userService.getJwtAuthorizes();
  }

}
