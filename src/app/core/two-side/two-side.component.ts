import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-two-side',
  templateUrl: './two-side.component.html',
  styleUrls: ['./two-side.component.scss']
})
export class TwoSideComponent implements OnInit {
  @Input('noleftside') noleftside:boolean;
  constructor() { }

  ngOnInit() {
  }

}
