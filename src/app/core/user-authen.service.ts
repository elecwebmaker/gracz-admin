import { Injectable } from '@angular/core';
import { CfUserHandlerJWTService } from 'app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service';
import { UrlProviderService, LOGIN, LOGOUT } from 'app/shared/url-provider.service';
import { Http } from '@angular/http/';
import { LoginUser } from 'app/core/login/login.component';

export interface ILoginData{
    name: string;
    picture_src: string;
    position: string;
}

@Injectable()
export class UserAuthenService {

    constructor(
        private cfUserJwtService: CfUserHandlerJWTService,
        private urlProvider: UrlProviderService,
        private http: Http
    ) { }

    login(user: LoginUser) {
        const url = this.urlProvider.getUrl(LOGIN);
        return this.http.post(url, {
            username: user.username,
            password: user.password
        }).map((res: any) => {
            this.cfUserJwtService.login(res.token, res.permission);
            if (user.savePass) {
                this.cfUserJwtService.saveToLocalStorage();
            }
        });
    }

    logout() {
        const url = this.urlProvider.getUrl(LOGOUT);
        return this.http.post(url, {}).map(() => {
            this.cfUserJwtService.logout();
        });
    }

    getToken() {
        return this.cfUserJwtService.getAccesstoken();
    }

    getUserData(): ILoginData {
        if (this.cfUserJwtService.isAuthen()) {
            return {
                name: this.cfUserJwtService.getUserData('name'),
                picture_src: this.cfUserJwtService.getUserData('picture_src'),
                position: this.cfUserJwtService.getUserData('position')
            };
        } else {
            return {
                name: undefined,
                picture_src: undefined,
                position: undefined
            };
        }
    }
}
