import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTabNavComponent } from './user-tab-nav.component';

describe('UserTabNavComponent', () => {
  let component: UserTabNavComponent;
  let fixture: ComponentFixture<UserTabNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTabNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTabNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
