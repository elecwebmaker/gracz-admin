import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-user-tab-nav',
    templateUrl: './user-tab-nav.component.html',
    styleUrls: ['./user-tab-nav.component.scss']
})
export class UserTabNavComponent implements OnInit {
    public user_tabList = [{
        value: '/user/list',
        text: 'User List'
    }, {
        value: '/user/archived',
        text: 'Archived List'
    }];

    constructor() { }

    ngOnInit() {
    }

}
