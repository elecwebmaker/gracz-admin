import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-order-tab-nav',
    templateUrl: './order-tab-nav.component.html',
    styleUrls: ['./order-tab-nav.component.scss']
})
export class OrderTabNavComponent implements OnInit {
    @Input() current: string;
    public order_tabList = [{
        value: '/model/list',
        text: 'สรุปข้อมูลการสั่งสินค้า',
        permission: 7
    }, {
        value: '/model/product/export',
        text: 'รายการส่งออก',
        permission: 10
    }];
    constructor() { }

    ngOnInit() {
    }
}
