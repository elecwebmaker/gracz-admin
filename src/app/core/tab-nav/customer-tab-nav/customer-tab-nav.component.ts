import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-customer-tab-nav',
    templateUrl: './customer-tab-nav.component.html',
    styleUrls: ['./customer-tab-nav.component.scss']
})
export class CustomerTabNavComponent implements OnInit {
    public cus_tabList = [{
        value: '/customer/list',
        text: 'รายการลูกค้า',
        permission: 4
    }, {
        value: '/customer/export',
        text: 'รายการส่งออก',
        permission: 6
    }];

    constructor() { }

    ngOnInit() {
    }

}
