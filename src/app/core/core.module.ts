import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './topbar/topbar.component';
import { AccountComponent } from './account/account.component';
import { MenuComponent } from './menu/menu.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TabComponent } from './tab/tab.component';
import {  SharedModule } from './../shared/shared.module';
import { OrderModule } from 'app/order/order.module';
import { CoreRoutingModule } from 'app/core/core.routing';
import { CustomerModule } from 'app/customer/customer.module';
import { RouterTabComponent } from './router-tab/router-tab.component';
import { InventoryModule } from 'app/inventory/inventory.module';
import { TwoSideComponent } from './two-side/two-side.component';
import { OrderTabNavComponent } from './tab-nav/order-tab-nav/order-tab-nav.component';
import { ProductlistSearchbarComponent } from './search-bar/productlist-searchbar/productlist-searchbar.component';
import { StatusComponent } from './status/status.component';
import { ModellistSearchbarComponent } from './search-bar/modellist-searchbar/modellist-searchbar.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { CraftutilityModule } from 'craftutility';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { KeywordListService } from './keyword-list.service';
import { ExportButtonComponent } from "app/core/export-button/export-button.component";
import { CustomerTabNavComponent } from './tab-nav/customer-tab-nav/customer-tab-nav.component';
import { UserAuthenService } from './user-authen.service';
import { LoginComponent } from './login/login.component';
import { LoginContainerComponent } from './login-container/login-container.component';
import { AccesstokenProviderService } from './accesstoken-provider.service';
import { CustomerBreadcrumbComponent } from './breadcrumb/customer-breadcrumb/customer-breadcrumb.component';
import { InventoryBreadcrumbComponent } from './breadcrumb/inventory-breadcrumb/inventory-breadcrumb.component';
import { OrderBreadcrumbComponent } from './breadcrumb/order-breadcrumb/order-breadcrumb.component';
import { TestloginComponent } from './testlogin/testlogin.component';
import { TestUserService } from './testuser.service';
import { TestLoginViewComponent } from './testloginview/testloginview.component';
import { AccountDetailPageComponent } from './account-detail-page/account-detail-page.component';
import { AccountDetailContainerComponent } from './account-detail-container/account-detail-container.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { EditBoxComponent } from './edit-box/edit-box.component';
import { EditBoxPhoneComponent } from './edit-box-phone/edit-box-phone.component';
import { AccountGetGetterService } from './account-get-getter.service';
import { AccountSetterService } from './account-setter.service';
import { ResetPassComponent } from './reset-pass/reset-pass.component';
import { ResetPassContainerComponent } from './reset-pass-container/reset-pass-container.component';
import { ResetPassPageComponent } from './reset-pass-page/reset-pass-page.component';
import { UserTabNavComponent } from './tab-nav/user-tab-nav/user-tab-nav.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AuthorizeCustomerGuard } from './guards/authorize-customer.guard';
import { AuthorizeOrderGuard } from './guards/authorize-order.guard';
import { AuthorizeInventoryGuard } from './guards/authorize-inventory.guard';
import { AuthorizeUserGuard } from './guards/authorize-user.guard';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CraftutilityModule,
    BrowserAnimationsModule,
  ],
  declarations: [TopBarComponent, AccountComponent, MenuComponent, SidebarComponent, TabComponent,TwoSideComponent,RouterTabComponent, OrderTabNavComponent, ProductlistSearchbarComponent, StatusComponent, ModellistSearchbarComponent, CustomerTabNavComponent, LoginComponent, LoginContainerComponent,ExportButtonComponent, CustomerBreadcrumbComponent, InventoryBreadcrumbComponent, OrderBreadcrumbComponent, TestloginComponent, TestLoginViewComponent,TestLoginViewComponent,AccountDetailPageComponent, AccountDetailContainerComponent, AccountDetailComponent, EditBoxComponent, EditBoxPhoneComponent, ResetPassComponent, ResetPassContainerComponent, ResetPassPageComponent, UserTabNavComponent, NotfoundComponent],
  exports: [TopBarComponent, SidebarComponent,TwoSideComponent,RouterTabComponent,TabComponent,OrderTabNavComponent,ProductlistSearchbarComponent,ModellistSearchbarComponent,ReactiveFormsModule,FormsModule,CraftutilityModule ,CustomerTabNavComponent,ExportButtonComponent,StatusComponent,CustomerBreadcrumbComponent, InventoryBreadcrumbComponent, OrderBreadcrumbComponent, UserTabNavComponent],
  providers: [KeywordListService, UserAuthenService, AccesstokenProviderService, TestUserService,AccountGetGetterService,AccountSetterService, AuthorizeCustomerGuard, AuthorizeOrderGuard, AuthorizeInventoryGuard, AuthorizeUserGuard]
})
export class CoreModule { }
