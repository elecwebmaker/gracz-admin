import { Igen } from './Igen';

export interface ICustomerExportModel {
    cus_id: number;
    cus_code: string;
    cus_name: string;
    cus_mc5: string;
}

export class CustomerExportModel implements Igen<ICustomerExportModel> {
    cus_id: number;
    cus_code: string;
    cus_name: string;
    cus_mc5: string;

    constructor(data: ICustomerExportModel) {
        this.cus_id = data.cus_id;
        this.cus_code = data.cus_code;
        this.cus_name = data.cus_name;
        this.cus_mc5 = data.cus_mc5;
    }

    gen(): ICustomerExportModel {
        return {
            cus_id: this.cus_id,
            cus_code: this.cus_code,
            cus_name: this.cus_name,
            cus_mc5: this.cus_mc5
        }
    }
}