import { Igen } from './Igen';

export interface IProductCardModel{
    doc_no: string;
    cus_name: string;
    status_text: string;
    product_code: string;
    price_text: string;
    default_price_text: string;
    total_price_text: string;
    quantity_text: string;
    pid: string;
    oid: string;
};

export class ProductCardModel implements Igen<IProductCardModel>{
    doc_no: string;
    cus_name: string;
    status_text: string;
    product_code: string;
    price_text: string;
    default_price_text: string;
    total_price_text: string;
    quantity_text: string;
    pid: string;
    oid: string;
    constructor(data:IProductCardModel){
        this.doc_no = data.doc_no;
        this.cus_name = data.cus_name;
        this.status_text = data.status_text;
        this.product_code = data.product_code;
        this.price_text = data.price_text;
        this.default_price_text = data.default_price_text;
        this.total_price_text = data.total_price_text;
        this.quantity_text = data.quantity_text;
        this.pid = data.pid;
        this.oid = data.oid;
    }

    gen():IProductCardModel{
        return {
             doc_no: this.doc_no,
            cus_name: this.cus_name,
            status_text: this.status_text,
            product_code: this.product_code,
            price_text: this.price_text,
            default_price_text: this.default_price_text,
            total_price_text: this.total_price_text,
            quantity_text: this.quantity_text,
            pid: this.pid,
            oid: this.oid
        }
    }
}