import { Igen } from './Igen';

export interface IProductModelModel{
    model_name: string;
    model_id: string;
    total_order:string;
    quantity:string;
    unit: string;

}

export class ProductModelModel implements Igen<IProductModelModel>{
    model_name:string;
    total_order: string;
    quantity: string;
    unit:string;   
    model_id:string;

    constructor(data:IProductModelModel){
        this.model_name = data.model_name;
        this.total_order = data.total_order;
        this.quantity = data.quantity;
        this.unit = data.unit;
        this.model_id = data.model_id;
    }

    gen():IProductModelModel{
        return {
            model_name: this.model_name,
            model_id: this.model_id,
            total_order: this.total_order,
            quantity: this.quantity,
            unit: this.unit,
        }
    }
}