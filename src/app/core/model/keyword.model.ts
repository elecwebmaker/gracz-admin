
import { Igen } from "app/core/model/Igen";
export interface IkeywordModel {
    id: string;
    text: string;
}

export class KeywordModel implements Igen<IkeywordModel>{

    id: string;
    text:string;

    constructor(data:IkeywordModel){
        this.id = data.id;
        this.text = data.text;
    }

    gen(): IkeywordModel {
        return {
            id: this.id,
            text: this.text
        }
    }
}