import { Igen } from './Igen';

export interface IList {
    id: number;
    text: string;
    isChk: boolean;
    required_text?: string;
}

export interface IPermission {
    sale: Array<IList>;
    sale_admin: Array<IList>;
}

export interface IUserDetail {
    id: number;
    username: string;
    name: string;
    pic_src: string;
    job_type: number;
}

export interface IUserPermissionPageModel {
    permission: IPermission;
    detail: IUserDetail;
}

export class UserPermissionPageModel implements Igen<IUserPermissionPageModel> {
    permission: IPermission;
    detail: IUserDetail;

    constructor(data: IUserPermissionPageModel) {
        this.permission = data.permission;
        this.detail = data.detail;
    }

    gen(): IUserPermissionPageModel {
        return {
            permission: this.permission,
            detail: this.detail
        }
    }
}