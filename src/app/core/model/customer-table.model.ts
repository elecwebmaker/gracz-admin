import { Igen } from './Igen';

export interface ICustomerTableModel {
    cus_id: string;
    cus_name: string;
    business_type: string;
    sale_name: string;
    status_text: string;
    cus_code: string;
}

export class CustomerTableModel implements Igen<ICustomerTableModel> {
    cus_id: string;
    cus_name: string;
    business_type: string;
    sale_name: string;
    status_text: string;
    cus_code: string;
    constructor(data: ICustomerTableModel) {
        this.cus_id = data.cus_id;
        this.cus_name = data.cus_name;
        this.business_type = data.business_type;
        this.sale_name = data.sale_name;
        this.status_text = data.status_text;
        this.cus_code = data.cus_code;
    }

    gen(): ICustomerTableModel {
        return {
            cus_id: this.cus_id,
            cus_name: this.cus_name,
            business_type: this.business_type,
            sale_name: this.sale_name,
            status_text: this.status_text,
            cus_code: this.cus_code
        }
    }
}