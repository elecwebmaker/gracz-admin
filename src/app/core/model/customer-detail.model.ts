import { Igen } from './Igen';

export interface IOrderHistory {
    doc_no: string;
    order_qty: string;
    status_text: string;
}

export interface ICustomerDetailModel {
    name: string;
    businessType: string;
    code: string;
    tel: Array<string>;
    sale_code: string;
    bill_addr: string;
    ship_addrs: Array<string>;
    sale_discount: string;
    payment_method: string;
    bill_condition: string;
    cheque_condition: string;
    credit_expected: string;
    month_expect: string;
    remark: string;
    registered_date: string;
    files: Array<{
      name: string;
      ref: string;
    }>;
    id_card: string;
    button: Array<{id: number, text: string}>;
}

export class CustomerDetailModel implements Igen<ICustomerDetailModel> {
    name: string;
    businessType: string;
    code: string;
    tel: Array<string>;
    id_card: string;
    sale_code: string;
    bill_addr: string;
    ship_addrs: Array<string>;
    sale_discount: string;
    payment_method: string;
    bill_condition: string;
    cheque_condition: string;
    credit_expected: string;
    month_expect: string;
    remark: string;
    registered_date: string;
    files: Array<{
      name: string;
      ref: string;
    }>;
    button: Array<{id: number, text: string}>;

    constructor(data: ICustomerDetailModel) {
        this.name = data.name;
        this.id_card = data.id_card;
        this.businessType = data.businessType;
        this.code = data.code;
        this.tel = data.tel;

        this.sale_code = data.sale_code;
        this.bill_addr = data.bill_addr;
        this.ship_addrs = data.ship_addrs;
        this.sale_discount = data.sale_discount;
        this.payment_method = data.payment_method;
        this.bill_condition = data.bill_condition;
        this.cheque_condition = data.cheque_condition;
        this.credit_expected = data.credit_expected;
        this.remark = data.remark;
        this.registered_date = data.registered_date;
        this.files = data.files;
        this.button = data.button;
        this.month_expect = data.month_expect;
    }

    gen(): ICustomerDetailModel {
        return {
            name: this.name,
            businessType: this.businessType,
            code: this.code,
            tel: this.tel,
            id_card:this.id_card,
            sale_code: this.sale_code,
            bill_addr: this.bill_addr,
            ship_addrs: this.ship_addrs,
            sale_discount: this.sale_discount,
            payment_method: this.payment_method,
            bill_condition: this.bill_condition,
            cheque_condition: this.cheque_condition,
            credit_expected: this.credit_expected,
            month_expect: this.month_expect,
            remark: this.remark,
            registered_date: this.registered_date,
            files: this.files,
            button: this.button
        };
    }

}