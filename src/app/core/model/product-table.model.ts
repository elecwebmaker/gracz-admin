import { Igen } from './Igen';

export interface IProductTableModel {
    name: string;
    product_code: string;
    quantity: number;
    unit: string;
    price: string;
    product_id: number;
    status_text: string;
}

export class ProductTableModel implements Igen<IProductTableModel> {
    name: string;
    product_code: string;
    quantity: number;
    unit: string;
    price: string;
    product_id: number;
    status_text: string;

    constructor(data: IProductTableModel) {
        this.name = data.name;
        this.product_code = data.product_code;
        this.quantity = data.quantity;
        this.unit = data.unit;
        this.price = data.price;
        this.product_id = data.product_id;
        this.status_text = data.status_text;
    }

    gen(): IProductTableModel {
        return {
            name: this.name,
            product_code: this.product_code,
            quantity: this.quantity,
            unit: this.unit,
            price: this.price,
            product_id: this.product_id,
            status_text: this.status_text
        }
    }
}