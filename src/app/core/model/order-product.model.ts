import { Igen } from './Igen';
import { IProductTableModel, ProductTableModel } from "app/core/model/product-table.model";
import { IProductCardDetail, ProductCardDetailModel } from "app/core/model/product-card-detail.model";

export interface IOrderProductModel {
    register_date: string;
    register_time: string;
    shipment_date: string;
    shipment_address: string;
    product_quantity: number;
    total_price_text: string;
    phone_number: Array<string>;
    remark: string;
    SO_qty: number;
    invoice_qty: number;
    product_card_detail: IProductCardDetail;
    product_table: Array<IProductTableModel>;
    button: Array<{id: number, text: string}>;
    model_id: string;
    model_code: string;
    unit: string;
}

export class OrderProductModel implements Igen<IOrderProductModel> {
    register_date: string;
    register_time: string;
    shipment_date: string;
    shipment_address: string;
    product_quantity: number;
    total_price_text: string;
    phone_number: Array<string>;
    remark: string;
    SO_qty: number;
    invoice_qty: number;
    product_card_detail: ProductCardDetailModel;
    product_table: Array<ProductTableModel>;
    button: Array<{id: number, text: string}>;
    model_id: string;
    model_code: string;
    unit: string;
    constructor(data: IOrderProductModel) {
        this.register_date = data.register_date;
        this.register_time = data.register_time;
        this.shipment_date = data.shipment_date;
        this.shipment_address = data.shipment_address;
        this.product_quantity = data.product_quantity;
        this.total_price_text = data.total_price_text;
        this.phone_number = data.phone_number;
        this.remark = data.remark;
        this.SO_qty = data.SO_qty;
        this.invoice_qty = data.invoice_qty;
        this.product_card_detail = new ProductCardDetailModel(data.product_card_detail);
        this.model_id = data.model_id;
        this.model_code = data.model_code;
        this.unit = data.unit;
        this.product_table = data.product_table.map((item) => {
            return new ProductTableModel(item);
        });
        this.button = data.button.map((item) => {
            return {
                id: item.id,
                text: item.text
            }
        });
    }

    gen(): IOrderProductModel {
        const tmp: Array<IProductTableModel> = this.product_table.map((item) => {
            return item.gen();
        });
        return {
            register_date: this.register_date,
            register_time: this.register_time,
            shipment_date: this.shipment_date,
            shipment_address: this.shipment_address,
            product_quantity: this.product_quantity,
            total_price_text: this.total_price_text,
            phone_number: this.phone_number,
            remark: this.remark,
            SO_qty: this.SO_qty,
            invoice_qty: this.invoice_qty,
            product_card_detail: this.product_card_detail.gen(),
            product_table: tmp,
            button: this.button,
            model_code:this.model_code,
            model_id: this.model_id,
            unit: this.unit
        };
    }

}