
import { Igen } from "app/core/model/Igen";

export interface ICustomerOrderModel {
    order_id: string;
    quantity: string;
    status_text: string;
}
export class CustomerOrderModel implements Igen<ICustomerOrderModel>{
    order_id:string;
    quantity: string;
    status_text: string;

    constructor(data:ICustomerOrderModel){
        this.order_id = data.order_id;
        this.quantity = data.quantity;
        this.status_text = data.status_text;
    }

    gen():ICustomerOrderModel{
        return {
            order_id: this.order_id,
            quantity: this.quantity,
            status_text: this.quantity
        }
    }
}