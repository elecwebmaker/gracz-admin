import { Igen } from './Igen';

export interface IProductCardDetail {
  category: string;
  model: string;
  unit: string;
  quantity: string;
  default_price: string;
  price: string;
  discount_per: string;
  discount_val: string;
  total_price: string;
  screen: string;
  doc_no: string;
  customer_name: string;
  status: number;
  status_text: string;
  product_code: string;
}

export class ProductCardDetailModel implements Igen<IProductCardDetail> {
  category: string;
  model: string;
  unit: string;
  quantity: string;
  default_price: string;
  price: string;
  discount_per: string;
  discount_val: string;
  total_price: string;
  screen: string;
  doc_no: string;
  customer_name: string;
  status: number;
  status_text: string;
  product_code: string;

  constructor(data: IProductCardDetail) {
    this.category = data.category;
    this.model = data.model;
    this.unit = data.unit;
    this.quantity = data.quantity;
    this.default_price = data.default_price;
    this.price = data.price;
    this.discount_per = data.discount_per;
    this.discount_val = data.discount_val;
    this.total_price = data.total_price;
    this.screen = data.screen;
    this.doc_no = data.doc_no;
    this.customer_name = data.customer_name;
    this.status = data.status;
    this.status_text = data.status_text;
    this.product_code = data.product_code;
  }

  gen(): IProductCardDetail {
    return {
      category: this.category,
      model: this.model,
      unit: this.unit,
      quantity: this.quantity,
      default_price: this.default_price,
      price: this.price,
      discount_per: this.discount_per,
      discount_val: this.discount_val,
      total_price: this.total_price,
      screen: this.screen,
      doc_no: this.doc_no,
      customer_name: this.customer_name,
      status: this.status,
      status_text: this.status_text,
      product_code: this.product_code
    }
  }
}