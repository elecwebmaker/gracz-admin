import { Igen } from './Igen';

export interface IOrderExportModel {
    order_id: number;
    preorder_id: string;
    customer_code: string;
    customer_name: string;
    shipment_date: string;
    total_price: string;
    product_id: string;
}

export class OrderExportModel implements Igen<IOrderExportModel> {
    order_id: number;
    preorder_id: string;
    customer_code: string;
    customer_name: string;
    shipment_date: string;
    total_price: string;
    product_id: string;
    constructor(data: IOrderExportModel) {
        this.order_id = data.order_id;
        this.preorder_id = data.preorder_id;
        this.customer_code = data.customer_code;
        this.customer_name = data.customer_name;
        this.shipment_date = data.shipment_date;
        this.total_price = data.total_price;
        this.product_id = data.product_id;
    }

    gen(): IOrderExportModel {
        return {
            order_id: this.order_id,
            preorder_id: this.preorder_id,
            customer_code: this.customer_code,
            customer_name: this.customer_name,
            shipment_date: this.shipment_date,
            total_price: this.total_price,
            product_id: this.product_id
        }
    }
}