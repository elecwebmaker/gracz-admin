import { Igen } from './Igen';

export interface IInventoryTableModel {
    fgcode: string;
    product_name: string;
    order_qty: string;
    sale_qty: string;
    remain_qty: string;
    unit: string;
}

export class InventoryTableModel implements Igen<IInventoryTableModel> {
    fgcode: string;
    product_name: string;
    order_qty: string;
    sale_qty: string;
    remain_qty: string;
    unit: string;

    constructor(data: IInventoryTableModel) {
        this.fgcode = data.fgcode;
        this.product_name = data.product_name;
        this.order_qty = data.order_qty;
        this.sale_qty = data.sale_qty;
        this.remain_qty = data.remain_qty;
        this.unit = data.unit;
    }

    gen(): IInventoryTableModel {
        return {
            fgcode: this.fgcode,
            product_name: this.product_name,
            order_qty: this.order_qty,
            sale_qty: this.sale_qty,
            remain_qty: this.remain_qty,
            unit: this.unit
        }
    }
}