import { Igen } from './Igen';

export interface IUserListModel {
    id: number;
    username: string;
    job_type: number;
    job_type_text?: string;
    name: string;
    phone: {code: string, number: string};
    email: string;
    line_id: string;
    pic: {
        url: string,
        tmp: string
    };
    isArchived: boolean;
    lastname: string;
    password: string;
    sale_mc5_code?: string;
}

export class UserListModel implements Igen<IUserListModel> {
    id: number;
    username: string;
    job_type: number;
    job_type_text: string;
    name: string;
    lastname: string;
    phone: {code: string, number: string};
    email: string;
    line_id: string;
    pic: {
        url: string,
        tmp: string
    };
    isArchived: boolean;
    password: string;
    sale_mc5_code: string;
    constructor(data: IUserListModel) {
        this.id = data.id;
        this.username = data.username;
        this.job_type = data.job_type;
        this.job_type_text = data.job_type_text;
        this.name = data.name;
        if(data.phone){
            this.phone = {
                code:data.phone.code,
                number:data.phone.number
            }
        }else{
            this.phone = <{code: string, number: string}>{};
        }
        this.email = data.email;
        this.line_id = data.line_id;
        if(data.pic){
            this.pic = {
                tmp:data.pic.tmp,
                url:data.pic.url,
            }
        }else{
            this.pic = <{url: string,tmp: string}> {};
        }
        this.isArchived = data.isArchived;
        this.lastname = data.lastname;
        this.password = data.password;
        this.sale_mc5_code = data.sale_mc5_code;
    }

    gen(): IUserListModel {
        return {
            id: this.id,
            username: this.username,
            job_type: this.job_type,
            job_type_text: this.job_type_text,
            name: this.name,
            phone: {
                code: this.phone.code,
                number: this.phone.number
            },
            email: this.email,
            line_id: this.line_id,
            pic: this.pic,
            isArchived: this.isArchived,
            lastname: this.lastname,
            password: this.password,
            sale_mc5_code: this.sale_mc5_code
        };
    }
}