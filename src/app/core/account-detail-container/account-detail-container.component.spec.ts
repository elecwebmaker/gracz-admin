import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailContainerComponent } from './account-detail-container.component';

describe('AccountDetailContainerComponent', () => {
  let component: AccountDetailContainerComponent;
  let fixture: ComponentFixture<AccountDetailContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
