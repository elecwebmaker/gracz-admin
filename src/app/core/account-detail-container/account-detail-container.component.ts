import { Component, OnInit } from '@angular/core';
import { IUserListModel, UserListModel } from "app/core/model/user-list.model";
import { AccountGetGetterService } from "app/core/account-get-getter.service";
import { CfDataDispatcher } from "app/shared/cf-comp-dispatcher/cf-data-dispatcher";
import { AccountSetterService } from "app/core/account-setter.service";
import { CfIContainerComp } from "app/shared/cf-comp-dispatcher/IcontainerComp";
import { NotifyService } from "craftutility";

@Component({
    selector: 'app-account-detail-container',
    templateUrl: './account-detail-container.component.html',
    styleUrls: ['./account-detail-container.component.scss']
})
export class AccountDetailContainerComponent implements OnInit,CfIContainerComp<UserListModel>{
    data_dispatcher: CfDataDispatcher<UserListModel> = new CfDataDispatcher<UserListModel>();
    constructor(
        private accountGet: AccountGetGetterService,
        private accountSet: AccountSetterService,
        private notifyservice: NotifyService
    ) { }

    ngOnInit() {
        this.accountGet.get().subscribe((data) => {
            data.pic.url = 'http://' + data.pic.url;
            this.data_dispatcher.setData(data);
        });
    }

    edit(newData) {
        this.accountSet.edit(newData).subscribe(()=>{
            this.notifyservice.showSuccess();
        });
    }

    uploadSuccess(newData){
        this.accountSet.edit({
            picture_src:newData
        }).subscribe(()=>{
            this.notifyservice.showUploadSuccess();
        });
    }

}
