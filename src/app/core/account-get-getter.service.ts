import { Injectable } from '@angular/core';
import { Http } from "@angular/http/";
import { UrlProviderService, ACCOUNT_GET } from "app/shared/url-provider.service";
import { UserListModel, IUserListModel } from "app/core/model/user-list.model";
import { Observable } from "rxjs/Rx";

@Injectable()
export class AccountGetGetterService {

    constructor(
        private http: Http,
        private urlprovider: UrlProviderService
    ) { }

    get(): Observable<UserListModel> {
        const url = this.urlprovider.getUrl(ACCOUNT_GET);
        return this.http.post(url, {}).map((res: any) => {
            const result = res.model;
            return new UserListModel({
                id: result.id,
                username: result.username,
                job_type: result.job_type,
                lastname:result.last_name,
                name: result.first_name,
                isArchived:false,
                password:undefined,
                phone: {
                    code: result.phone_code,
                    number: result.phone_number
                },
                email: result.email,
                line_id: result.line_id,
                pic: {
                    url:result.picture_src,
                    tmp:"",
                }
            });
        });
    }

}
