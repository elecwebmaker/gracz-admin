import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  @Output() onChange: EventEmitter<any> = new EventEmitter<any>(undefined);
  selectinput = new FormControl('none');
  @Input() statuslist: Array<{value:any,text:any}> = [];
  constructor() { }

  ngOnInit() {
    this.selectinput.valueChanges.subscribe((val)=>{
      if(val == 'none'){
         this.onChange.emit(undefined);
      }else{
         this.onChange.emit(val);
      }
     
    })
  }

}
