import { TestBed, inject } from '@angular/core/testing';

import { AccountSetterService } from './account-setter.service';

describe('AccountSetterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountSetterService]
    });
  });

  it('should ...', inject([AccountSetterService], (service: AccountSetterService) => {
    expect(service).toBeTruthy();
  }));
});
