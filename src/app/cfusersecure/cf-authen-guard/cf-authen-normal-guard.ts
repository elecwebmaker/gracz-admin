import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { CfUserHandlerNormalService } from "app/cfusersecure/cf-user-handler/cf-user-handler-normal.service";
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";

@Injectable()
export class CfAuthenNormalGuard implements CanActivate {
    constructor(private cfuserjwtmanage: CFAuthenJWTManageService,private cfuserhandlerNormalservice :CfUserHandlerNormalService,private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const isAuthen = this.cfuserhandlerNormalservice.isAuthen();
        if(!isAuthen){
            this.router.navigate([this.cfuserjwtmanage.routeWhenUnAuthorize]);
        }
        return isAuthen;
    }
}