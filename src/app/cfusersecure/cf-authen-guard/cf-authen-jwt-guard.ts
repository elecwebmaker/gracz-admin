import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";

@Injectable()
export class CfAuthenJWTGuard implements CanActivate {
    constructor(private cfuserhandlerJWTservice :CfUserHandlerJWTService,private router: Router,private cfuserjwtmanage: CFAuthenJWTManageService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const isAuthen = this.cfuserhandlerJWTservice.isAuthen();
        if(!isAuthen){
            this.router.navigate([this.cfuserjwtmanage.routeWhenInvalid]);
        }
        return isAuthen;
    }
}