import { Injectable } from '@angular/core';

@Injectable()
export class CFAuthenJWTManageService {
    routeWhenExpired: string = '/';
    routeWhenInvalid: string = '/';
    routeWhenUnAuthorize: string = '/';
    constructor() { 

    }

    setRouteWhenExpired(routeWhenExpired: string){
        this.routeWhenExpired = routeWhenExpired;
    }

    setRouteWhenInvalid(routeWhenInvalid){
        this.routeWhenInvalid = routeWhenInvalid;
    }

    setRouteWhenUnAuthorize(routeWhenUnAuthorize){
        this.routeWhenUnAuthorize = routeWhenUnAuthorize;
    }
    
}