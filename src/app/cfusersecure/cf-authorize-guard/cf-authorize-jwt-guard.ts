import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";

@Injectable()
export class CfAuthorizeJWTGuard {
    private hasAuthorize: boolean;
    constructor(
        private cfuserhandler: CfUserHandlerJWTService,
        private cfauthenmanage: CFAuthenJWTManageService,
        private router: Router
    ) { }

    isAuthorize(permission, overideRoute?) {
        if (permission instanceof Array) {
            console.log(permission.length)
            if (permission.length !== 0) {
                this.hasAuthorize = permission.some((item)=>{
                    return this.cfuserhandler.isAuthorize(item);
                });
            }
        } else {
            this.hasAuthorize = this.cfuserhandler.isAuthorize(permission);
        }
        if (!this.hasAuthorize) {
            if (overideRoute) {
                this.router.navigate([overideRoute]);
            } else {
                this.router.navigate([this.cfauthenmanage.routeWhenUnAuthorize]);
            }
        }
        return this.hasAuthorize;
    }

}