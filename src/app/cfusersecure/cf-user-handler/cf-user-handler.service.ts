import { CfUserSecure } from "app/cfusersecure/cf-user-secure/cf-user-secure";

export abstract class CfUserHandlerService {
    protected abstract cfUserSercure: CfUserSecure;
    protected _login(accesstoken: string, authorizes?:Array<any>): void{
        this.cfUserSercure.login(accesstoken,authorizes);
    }
    
    logout(){
        this.cfUserSercure.logout();
    }

    getAccesstoken(): string{
        return this.cfUserSercure.user.accesstoken.getAccesstoken();
    }

    getUserData(key: string): any{
        return this.cfUserSercure.user.get(key);
    }

    isAuthen(){
        return this.cfUserSercure.isAuthen();
    }
    
    saveToLocalStorage(){
        this.cfUserSercure.saveToLocalStorage();
    }

    isAuthorize(authorize:any){
        return this.cfUserSercure.isAuthorize(authorize);
    }

    getAuthorize(){
        return this.cfUserSercure.user.authorize.getAuthorize();
    }
}