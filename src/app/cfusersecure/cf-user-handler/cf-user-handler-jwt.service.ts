import { CfUserHandlerService } from "app/cfusersecure/cf-user-handler/cf-user-handler.service";
import { CfUserSecureJWT } from "../cf-user-secure/cf-user-secure-jwt";

import { Injectable } from '@angular/core';
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";
import { Router } from "@angular/router";

@Injectable()
export class CfUserHandlerJWTService extends CfUserHandlerService {
    cfUserSercure = new CfUserSecureJWT();

    constructor(private cfauthenmanage: CFAuthenJWTManageService,private router: Router){
        super();
    }


    login(accesstoken: string,authorize?: Array<any>){
      
        this._login(accesstoken,authorize);
        this.cfUserSercure.genUserData();
    }

    isAuthen(){
        
        let isAuthen;
        try{
            isAuthen = this.cfUserSercure.isAuthen();
        }
        catch(e){
            switch(e.name){
                case 'TOKEN_IS_INVALID':
                    console.log('router to -> ',this.cfauthenmanage.routeWhenInvalid);
                    this.router.navigate([this.cfauthenmanage.routeWhenInvalid]);
                    
                    break;
                case 'TOKEN_EXPIRED':
                     console.log('EXPIRED! router to -> ',this.cfauthenmanage.routeWhenExpired);
                    this.router.navigate([this.cfauthenmanage.routeWhenExpired]);
                    break;
            }
                

            this.logout();
        }
        if(!isAuthen){
            // this.router.navigate([this.cfauthenmanage.routeWhenUnAuthorize]);
        }
        return isAuthen;
    }
}