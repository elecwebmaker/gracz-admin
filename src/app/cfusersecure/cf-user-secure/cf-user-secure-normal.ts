import { CfUserSecure } from "app/cfusersecure/cf-user-secure/cf-user-secure";
import { CfUser } from "app/cfusersecure/cfuser/cf-user";

export class CfUserSecureNormal extends CfUserSecure {
    
    saveUser(userdata: Object): void{
        this.setUser(userdata);
    }

    setUser(props: Object){
        this._setUser(props);
        this.session.save(this.user);
    }

    saveToLocalStorage(){
        this.local.save(this.user);
    }

    setUserFromLocal(){
        const cfuser = this.local.get();
        this.user = cfuser;
    }

    setUserFromSession(){
        const cfuser = this.session.get();
        this.user = cfuser;
    }
}