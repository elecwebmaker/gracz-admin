import { Directive, ElementRef, Input, Renderer2, ViewContainerRef, TemplateRef, OnChanges, EmbeddedViewRef } from '@angular/core';
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";

@Directive({
    selector: '[cfdisplaysecure]'
})
export class CfDisplaySecureDirective implements OnChanges{
    @Input() cfdisplaysecure: any;
    viewRef: EmbeddedViewRef<any>;
    hasAuthorize: boolean
    constructor(
        private _tem: TemplateRef<any>,
        private _view: ViewContainerRef,
        private cfJwtGuard: CfUserHandlerJWTService
    ) { }

    ngOnChanges() {
        if (this.cfdisplaysecure === undefined) {
            this.hasAuthorize = true;
        } else if (this.cfdisplaysecure instanceof Array) {
            if (this.cfdisplaysecure.length !== 0) {
                // console.log(this.cfdisplaysecure.some(this.cfJwtGuard.isAuthorize))
                this.hasAuthorize = this.cfdisplaysecure.some((item)=>{
                    return this.cfJwtGuard.isAuthorize(item);
                });
            }
        } else {
            this.hasAuthorize = this.cfJwtGuard.isAuthorize(this.cfdisplaysecure);
        }

        if (!this.hasAuthorize) {
            this.clear();
        }else{
            this.create();
        }
    }

    private clear(){
        if(this.viewRef){
            this._view.clear();
        }
    }

    private create(){
        this.clear();
        this.viewRef = this._view.createEmbeddedView(this._tem);
    }


}
