import { NgModule } from '@angular/core';
import { CFAuthenJWTManageService } from "app/cfusersecure/cf-authen-manage/cf-authen-jwt-manage.service";
import { CfUserHandlerNormalService } from "app/cfusersecure/cf-user-handler/cf-user-handler-normal.service";
import { CfAuthenNormalGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-normal-guard";
import { CfAuthenJWTGuard } from "app/cfusersecure/cf-authen-guard/cf-authen-jwt-guard";
import { CfUserHandlerJWTService } from "app/cfusersecure/cf-user-handler/cf-user-handler-jwt.service";
import { CfDisplaySecureDirective } from './cf-display-secure.directive';
import { CfUserHandlerService } from "app/cfusersecure/cf-user-handler/cf-user-handler.service";
import { CfAuthorizeJWTGuard } from "app/cfusersecure/cf-authorize-guard/cf-authorize-jwt-guard";

@NgModule({
    imports: [],
    exports: [CfDisplaySecureDirective],
    declarations: [CfDisplaySecureDirective],
    providers: [CFAuthenJWTManageService, CfUserHandlerNormalService, CfAuthenNormalGuard,CfAuthenJWTGuard, CfUserHandlerJWTService, CfAuthorizeJWTGuard],
})
export class CfSecureModule { }
