export class CfAuthorize{
    authorizes: Array<any> = [];
    constructor(authorizes:Array<any> = []){
        if(authorizes){
            this.authorizes = authorizes;
        }
    }

    setAuthorize(authorizes:Array<any>):void{
        this.authorizes = authorizes;
    }

    isAuthorized(authorize:any):boolean{
        return this.authorizes.includes(authorize);
    }

    getAuthorize(){
        return this.authorizes;
    }
}