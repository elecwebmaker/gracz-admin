import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';

import { AppComponent } from './app.component';
import { CoreModule } from 'app/core/core.module';
import { SharedModule } from "app/shared/shared.module";
import { AppRoutingModule } from "app/app.routing";
import { OrderModule } from './order/order.module';
import { CustomHttp } from 'craftutility';
import { CustomerModule } from "app/customer/customer.module";
import { InventoryModule } from "app/inventory/inventory.module";
import { AccesstokenProviderService } from "app/core/accesstoken-provider.service";
import { UserModule } from "app/user/user.module";
import {CraftHttpNotifyService } from 'craftutility';

export function CustomhttpProvider(backend, defaultOptions: any, accesstoken: AccesstokenProviderService,crafthttpnotify: CraftHttpNotifyService){
  return new CustomHttp(backend, defaultOptions, '',crafthttpnotify, accesstoken, 'token');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    OrderModule,
    CustomerModule,
    InventoryModule,
    UserModule,

  ],
  providers: [
    { provide: Http,
      useFactory: CustomhttpProvider,
      deps: [XHRBackend, RequestOptions, AccesstokenProviderService,CraftHttpNotifyService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
