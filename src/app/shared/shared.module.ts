import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSemanticModule } from 'ng-semantic/ng-semantic';
import { CFTableControlModule, CFTableModule } from 'craft-table';
import { CraftutilityModule, CfInvalidControlModule } from 'craftutility';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UrlProviderService } from './url-provider.service';
import { CfDisplayManagerComponent } from 'craftutility';
import { DetailBoxComponent } from './detail-box/detail-box.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { CfBreadcrumbComponent } from './cf-breadcrumb/cf-breadcrumb/cf-breadcrumb.component';
import { CfSecureModule } from 'app/cfusersecure/cf-secure.module';
import { PrefixCountryCodeDirective } from './prefix-country-code.directive';
import { CfUploadImageService } from './cf-upload/cf-upload-image.service';
import { CfUploadImageComponent } from './cf-upload/cf-upload-image/cf-upload-image.component';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload/ng2-file-upload';
import { CfPreviewImageComponent } from './cf-upload/cf-preview-image/cf-preview-image.component';
import { CfSlidebarComponent } from './cf-slidebar/cf-slidebar.component';
import { CfTestdisplayManagerComponent } from "app/shared/cf-comp-dispatcher/cf-display-manager/cf-display-manager.component";
import { NoDataComponent } from './no-data/no-data.component';
import { LoadingComponent } from './loading/loading.component';
import { PaginationEveComponent } from './../pagination-eve/pagination-eve.component';
import { SizinationEveComponent } from './../sizination-eve/sizination-eve.component';
import { NavigationEveComponent } from './../navigation-eve/navigation-eve.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    NgSemanticModule,
    CraftutilityModule,
    CFTableModule,
    ReactiveFormsModule,
    RouterModule,
    CfSecureModule,
    CfInvalidControlModule,
    FormsModule
  ],
  exports:[
    NgSemanticModule,
    CFTableModule,
    CraftutilityModule,
    ReactiveFormsModule,
    RouterModule,
    DetailBoxComponent,
    ConfirmModalComponent,
    CfBreadcrumbComponent,
    CfInvalidControlModule,
    PrefixCountryCodeDirective,
    CfUploadImageComponent,
    FileSelectDirective,
    FileDropDirective,
    CfPreviewImageComponent,
    CfSecureModule,
    CfSlidebarComponent,
    CfTestdisplayManagerComponent,
    NoDataComponent,
    LoadingComponent,
    PaginationEveComponent,
    SizinationEveComponent,
    NavigationEveComponent
  ],
  declarations: [
    DetailBoxComponent,
    ConfirmModalComponent, 
    CfBreadcrumbComponent, 
    PrefixCountryCodeDirective, 
    CfUploadImageComponent,
    FileSelectDirective,
    FileDropDirective,
    CfPreviewImageComponent,
    CfSlidebarComponent,
    CfTestdisplayManagerComponent,
    NoDataComponent,
    LoadingComponent,
    PaginationEveComponent,
    SizinationEveComponent,
    NavigationEveComponent
  ],
  providers:[UrlProviderService, CfUploadImageService]
})
export class SharedModule { }
