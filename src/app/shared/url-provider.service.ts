import { Injectable } from '@angular/core';
import { API_URL,isMock } from './../../config';
import { CfUrlprovider } from 'craftutility';

export const PRODUCT_LIST = 'PRODUCT_LIST';
export const MODEL_LIST = 'MODEL_LIST';
export const CUSTOMER_LIST = 'CUSTOMER_LIST';
export const CUSTOMER_GET = 'CUSTOMER_GET';
export const ORDER_LIST_BY_CUSTOMER = 'ORDER_LIST_BY_CUSTOMER';
export const ORDER_PRODUCT_GET = 'ORDER_PRODUCT_GET';

export const LIST_MODEL_UNIT = 'LIST_MODEL_UNIT';
export const LIST_PRODUCT_STATUS = 'LIST_PRODUCT_STATUS';
export const LIST_INVENTORY_UNIT = 'LIST_INVENTORY_UNIT';
export const LIST_CUSTOMER_STATUS = 'LIST_CUSTOMER_STATUS';
export const LIST_JOB_TYPE = 'LIST_JOB_TYPE';
export const LIST_PERMISSION = 'LIST_PERMISSION';

export const INVENTORY_LIST = 'INVENTORY_LIST';
export const ORDER_EXPORT_LIST = 'ORDER_EXPORT_LIST';
export const CUSTOMER_EXPORT_LIST = 'CUSTOMER_EXPORT_LIST';

export const CUSTOMER_EXPORT_SEND = 'CUSTOMER_EXPORT_SEND';
export const ORDER_EXPORT_SEND = 'ORDER_EXPORT_SEND';
export const CUSTOMER_SET_STATUS = 'CUSTOMER_SET_STATUS';
export const PRODUCT_SET_STATUS = 'PRODUCT_SET_STATUS';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const USER_ADD = 'USER_ADD';
export const USER_GET = 'USER_GET';
export const USER_EDIT = 'USER_EDIT';
export const UPLOAD_FILE = 'UPLOAD_FILE';
export const ACCOUNT_GET = 'ACCOUNT_GET';
export const ACCOUNT_EDIT = 'ACCOUNT_EDIT';

export const GEN_PASSWORD = 'GEN_PASSWORD';
export const ACCOUNT_LIST = 'ACCOUNT_LIST';
export const PASSWORD_RESET = 'PASSWORD_RESET';
export const ARCHIVED_SET = 'ARCHIVED_SET';
export const PERMISSION_SET = 'PERMISSION_SET';
export const PERMISSION_GET = 'PERMISSION_GET';
export const CHK_CODE_MC5 = 'CHK_CODE_MC5';

export const PRODUCT_DETAIL_LIST = 'PRODUCT_DETAIL_LIST';
@Injectable()
export class UrlProviderService extends CfUrlprovider{
  api_url: string = API_URL;
  is_mock: boolean = isMock;

  constructor() {
    super();

    this.setUrl(PRODUCT_LIST,'/product/groupModelList','');
    this.setUrl(MODEL_LIST,'/inventory/groupModelList','');
    this.setUrl(CUSTOMER_LIST,'/customer/adminList','');
    this.setUrl(CUSTOMER_GET,'/customer/get','');
    this.setUrl(ORDER_LIST_BY_CUSTOMER,'/order/adminList','');
    this.setUrl(INVENTORY_LIST,'/inventory/list','');

    this.setUrl(LIST_MODEL_UNIT,'/inventory/unitList','http://demo0905840.mockable.io/getmockkeyword');
    this.setUrl(LIST_PRODUCT_STATUS,'/product/statusMapping','http://demo0905840.mockable.io/getmockkeyword');
    this.setUrl(LIST_INVENTORY_UNIT,'/inventory/categoryList','http://demo0905840.mockable.io/getmockkeyword');
    this.setUrl(LIST_CUSTOMER_STATUS,'/customer/statusMapping','http://demo0905840.mockable.io/getmockkeyword');
    this.setUrl(ORDER_EXPORT_LIST, '/order/exportList', '');
    this.setUrl(CUSTOMER_EXPORT_LIST, '/customer/exportList', '');
    this.setUrl(ORDER_PRODUCT_GET,'/order/getDetail','');
    this.setUrl(CUSTOMER_EXPORT_SEND, '/customer/editExportStatus','');
    this.setUrl(ORDER_EXPORT_SEND,'/order/editExportStatus','')
    this.setUrl(CUSTOMER_SET_STATUS, '/customer/editStatus', '');
    this.setUrl(PRODUCT_SET_STATUS, '/product/editStatus', '');
    this.setUrl(LOGIN, '/user/login', '');
    this.setUrl(LOGOUT, '/user/logout', '');
    this.setUrl(USER_ADD,'/user/add','');
    this.setUrl(USER_EDIT,'/user/edit/id','');
    this.setUrl(UPLOAD_FILE,'/customer/uploadFile', 'http://162.243.199.82/mju-co-op/api/member/uploadFile');

    this.setUrl(ACCOUNT_GET, '/user/getTokenDetail', '');
    this.setUrl(USER_GET,'/user/get/id','');
    this.setUrl(GEN_PASSWORD,'/user/generatePassword','');
    this.setUrl(ACCOUNT_EDIT, '/user/selfEdit', '');
    this.setUrl(PASSWORD_RESET, '/user/changePassword', '');
    this.setUrl(ACCOUNT_LIST, '/user/list', '');
    this.setUrl(ARCHIVED_SET, '/user/editArchivedStatus', '');
    this.setUrl(LIST_JOB_TYPE, '/user/jobTypeList', '');
    this.setUrl(LIST_PERMISSION, '/user/permissionList', '');
    this.setUrl(PERMISSION_SET, '/user/editPermission', '');
    this.setUrl(PERMISSION_GET, '/user/getPermission', '');
    this.setUrl(CHK_CODE_MC5, '/user/checkSaleCode','');

    this.setUrl(PRODUCT_DETAIL_LIST, '/order/getDetailList','')
  }

}
