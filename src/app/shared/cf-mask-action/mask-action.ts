import { ContentChild, AfterContentInit } from '@angular/core';
import { MaskPresentComp } from 'app/shared/cf-mask-action/mask-present-comp';

export abstract class MaskAction {
    @ContentChild(MaskPresentComp) presentComp: MaskPresentComp;

    ngAfterContentInit() {
        this.presentComp.maskValueChanges.subscribe((item) => {
            this.action(item);
        });
    }

    abstract action(value): void;
}