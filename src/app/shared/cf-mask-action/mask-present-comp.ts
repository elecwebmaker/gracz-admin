import { Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';

export class MaskPresentComp {
    @Output() maskValueChanges: EventEmitter<any> = new EventEmitter();
}