import { Directive, ElementRef, HostListener, Input, Renderer2, OnInit, Provider, forwardRef, } from '@angular/core';
import { FormControl, NgControl, NG_VALUE_ACCESSOR ,ControlValueAccessor} from "@angular/forms";



@Directive({
    selector: '[prefixCountryCode]',
    providers:[]
})
export class PrefixCountryCodeDirective implements OnInit{
    private _onChange:Function;
    private _onTouched:Function;
    @Input('formControl') formcontrol: FormControl;
    constructor(
        private el: ElementRef,
        private render: Renderer2,
    ) { }
    ngOnInit(){
        this.changeValuedown();
    }
    setMask(val){
        var curr_val =  val;
        let phonePattern = /[\+\*\-\/\=\.]/g;
        curr_val = curr_val.replace(phonePattern, '');
        if (curr_val != ''){
            curr_val = '+' + curr_val;
        }
        return curr_val;
    }
     @HostListener('input') changeValuedown() {
            let curr = this.setMask(this.el.nativeElement.value);
            this.formcontrol.setValue(curr);
    }


}
