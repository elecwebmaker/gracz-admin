import {Component, OnInit, Input, ViewChild, ElementRef, forwardRef} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms/';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CfSlidebarComponent),
  multi: true
};

@Component({
  selector: 'cf-slidebar',
  templateUrl: './cf-slidebar.component.html',
  styleUrls: ['./cf-slidebar.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CfSlidebarComponent implements OnInit, ControlValueAccessor {
  @Input() step = 1;
  @Input() range: Array<number> = [];
  @Input() hasSnapPoint = false;
  @ViewChild('bar') slideBar: ElementRef;
  @ViewChild('box') slideBarBox: ElementRef;
  hasDisplay: boolean;
  start: number;
  end: number;
  point: number;
  snaps: Array<number> = [];
  chkSnapBined = this.chkSnap.bind(this);
  private _value = 0;
  private _isDisabled = false;
  private _onChange = (_: any) => {};
  private _onTouched = () => {};
  constructor() {}

  ngOnInit() {
    this.setSnap(this.range[0], this.range[1]);
    this.start = this.slideBarBox.nativeElement.offsetLeft;
    this.end = this.start + this.slideBarBox.nativeElement.clientWidth;
    window.addEventListener('mouseup', () => {
      this.hideValueDisplay();
      window.removeEventListener('mousemove', this.chkSnapBined);
    });
    window.addEventListener('keydown', event => {
      if (event.target === this.slideBarBox.nativeElement) {
        if (event.keyCode === 38 || event.keyCode === 39) {
          this.increseSlidebar();
        } else if (event.keyCode === 37 || event.keyCode === 40) {
          this.decreseSlidebar();
        }
      }
    });
    this.updatePoint();
  }

  setSnap(start: number, end: number) {
    const width = this.slideBar.nativeElement.clientWidth;
    const snapDistance = width / (end - start);
    for (let i = 0; i <= end - start; i += this.step) {
      this.snaps.push(i * snapDistance);
    }
  }

  addEvent(e) {
    if (!this._isDisabled) {
      this.chkSnap(e);
      window.addEventListener('mousemove', this.chkSnapBined);
      this.slideBarBox.nativeElement.addEventListener(
        'touchmove',
        this.chkSnapBined
      );
      this.showValueDisplay();
    }
  }

  removeEvent() {
    console.log('end');
    this.slideBarBox.nativeElement.removeEventListener(
      'touchmove',
      this.chkSnapBined
    );
    this.hideValueDisplay();
  }

  chkSnap(event) {
    const pos =
      (event.touches ? event.touches[0].clientX : event.clientX) - this.start;
    let ans: number;
    for (let i = 0; i <= this.snaps.length; i++) {
      if (ans > Math.abs(pos - this.snaps[i]) || i === 0) {
        ans = Math.abs(pos - this.snaps[i]);
      } else {
        this.value = i * this.step - this.step;
        this.updatePoint();
        break;
      }
    }
  }

  scrollY(event) {
    if (event.deltaY < 0) {
      this.increseSlidebar();
    } else if (event.deltaY > 0) {
      this.decreseSlidebar();
    }
  }

  increseSlidebar() {
    if (this.value < this.range[1]) {
      this.value += this.step;
      this.updatePoint();
    }
  }

  decreseSlidebar() {
    if (this.value > this.range[0]) {
      this.value -= this.step;
      this.updatePoint();
    }
  }

  updatePoint() {
    this.point = this.value / this.step;
  }

  showValueDisplay() {
    this.hasDisplay = true;
  }

  hideValueDisplay() {
    this.hasDisplay = false;
  }

  get value(): number {
    return this._value;
  }

  set value(val: number) {
    if (this._value !== val) {
      this._value = val;
      this._onChange(val);
    }
  }

  writeValue(value: number): void {
    if (!value) {
      return;
    }
    if (this.value !== value) {
      this.value = value;
      this.updatePoint();
    }
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this._isDisabled = isDisabled;
  }
}
