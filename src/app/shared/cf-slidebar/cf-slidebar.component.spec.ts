import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfSlidebarComponent } from './cf-slidebar.component';

describe('CfSlidebarComponent', () => {
  let component: CfSlidebarComponent;
  let fixture: ComponentFixture<CfSlidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfSlidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfSlidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
