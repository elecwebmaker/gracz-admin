import { TestBed, inject } from '@angular/core/testing';

import { CfUploadImageService } from './cf-upload-image.service';

describe('CfUploadImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CfUploadImageService]
    });
  });

  it('should ...', inject([CfUploadImageService], (service: CfUploadImageService) => {
    expect(service).toBeTruthy();
  }));
});
