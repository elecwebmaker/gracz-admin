import { Component, OnInit, EventEmitter, Output, ElementRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { Itemupload, CfUploadImageService } from "app/shared/cf-upload/cf-upload-image.service";
import { FileUploader, FileItem } from "ng2-file-upload";

@Component({
  selector: 'app-cf-upload-image',
  templateUrl: './cf-upload-image.component.html',
  styleUrls: ['./cf-upload-image.component.scss']
})
export class CfUploadImageComponent implements OnInit {
  uploader: FileUploader;
  @Output() onUpload: EventEmitter<any> = new EventEmitter<any>();
  @Input() imagepreview: TemplateRef<any>;
  @Input() profile_src: string;
  @Output() success: EventEmitter<any> = new EventEmitter<any>();
  @Output() error: EventEmitter<any> = new EventEmitter<any>();
  @Input() url: string;
  @Input() imageinital: string;
  @ViewChild('fileinput') fileinput: ElementRef;
  constructor(private uploaderservice: CfUploadImageService) {
    this.uploader = this.uploaderservice.getInstance();
  }
  img_src: string;
  imageContext:any = { 
    $implicit:{}
  };
  ngOnInit() {
    console.log('1');
    this.createImageContext({},false,true);
    this.uploaderservice.setUrl(this.url);
    this.uploaderservice.upload$.subscribe((res: any) => {
      this.img_src = res.url;
      this.onUpload.emit(res);
      console.log('2');
      this.createImageContext(res,false,true);
      this.success.next(res);
    },()=>{
      this.error.next();
    });
    var that_cf = this;
    console.log('3');
    this.uploader.onBeforeUploadItem = function(){
      console.log(that_cf,'that!');
      // this.createImageContext.apply(that_cf,[{},true,false]);
    }
    
  }
  
  get itemUpload(){
    return this.uploaderservice.queue();
  }

  openFileUpload(){
    let event = new MouseEvent('click', {bubbles: true});
    this.fileinput.nativeElement.dispatchEvent(event);
  }


  createImageContext(res,loading,binding:boolean){
    console.log(this,'this')
    if(!this.imageContext){
      this.imageContext = {};
    }
    this.imageContext['$implicit'] = res;
    this.imageContext['loading'] = loading;
    if(binding){
        this.imageContext.openFileUpload = this.openFileUpload.bind(this)
    }
    console.log(this.imageContext,'imageContext');
      
  }

}
