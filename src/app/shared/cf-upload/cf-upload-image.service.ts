import { Injectable } from '@angular/core';
import { FileUploader, FileItem } from "ng2-file-upload";
import { Subject } from "rxjs/Rx";
import { NotifyService } from "craftutility";
import { UrlProviderService, UPLOAD_FILE } from "app/shared/url-provider.service";
export interface Itemupload {
  name: string;
  url: string;
  id?: string;
};//refactor
@Injectable()
export class CfUploadImageService {
  upload$:Subject<any> = new Subject<any>();
  url: string;
  private uploader: FileUploader = new FileUploader({
    autoUpload:true
  });

  setUrl(url){
    this.url = url;
    this.uploader.setOptions({url:this.url});
  }
  constructor(private urlprovider: UrlProviderService,private notifyservice: NotifyService) {
    this.uploader.onSuccessItem = (item,res) =>{
      const resjson = JSON.parse(res);
      // const { absolute, original_name } = resjson;
      this.upload$.next(resjson);
    };
    this.uploader.onErrorItem = () => {
      this.upload$.error({});
    };
  };

  upload(item: FileItem){
    item.method = 'POST';
    item.upload();
  };

  queue(){
    return this.uploader.queue[0];
  }

  getInstance() {
    return this.uploader;
  }
}
