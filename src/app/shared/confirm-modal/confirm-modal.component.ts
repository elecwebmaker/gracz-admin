import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Subject } from "rxjs/Rx";
import { SemanticModalComponent } from "ng-semantic/ng-semantic";

@Component({
  selector: 'cf-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  comfirm$ : Subject<boolean> = new Subject<boolean>();
  @ViewChild('confirmModal') confirmModal: SemanticModalComponent;
  @Input() title: string;
  constructor() { }

  ngOnInit() {
  }

  private confirm(){
    this.comfirm$.next(true);
    this.confirmModal.hide();
  }

  private cancel(){
    this.comfirm$.next(false);
    this.confirmModal.hide();
  }

  show(){
    this.confirmModal.show();
  }

  hide(){
    this.confirmModal.hide();
  }
}
