import { Input,OnInit} from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';

export class CfPresentComponent<T> implements OnInit{
    @Input() data: T;
    @Input() loading: BehaviorSubject<boolean>;
    public is_loading: boolean;
    private _hasdata: boolean;

    stateChange$ = new BehaviorSubject<boolean>(false);

    public hasData(): boolean {
        return this._hasdata;
    }

    private setHasData(hasData): void {
        this._hasdata = hasData;
    }

    constructor(){
        
    }

    OnInit(){

    }
    
    ngOnInit(){
        this.subLoading();
        this.OnInit();
    }

    private subLoading(): void {
        this.loading.subscribe((item) => {
            setTimeout(()=>{
                this.is_loading = item;
                if(!item && this.data){
                    this.setHasData(true);
                    if(this.data instanceof Array && this.data.length <= 0){
                        this.setHasData(false);
                    }
                }else{
                    this.setHasData(false);
                }
                this.stateChange$.next(item);
            },0);
        });
    }
}