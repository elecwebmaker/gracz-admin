import { Component, OnInit, Input, ContentChild, AfterContentInit } from '@angular/core';
import { CfPresentComponent } from 'app/shared/cf-comp-dispatcher/cf-present-comp';
import { CfDisplayState } from 'app/shared/cf-comp-dispatcher/cf-display-state';

@Component({
    selector: 'cf-testdisplay-manager',
    templateUrl: './cf-display-manager.component.html',
    styleUrls: ['./cf-display-manager.component.scss']
})
export class CfTestdisplayManagerComponent implements AfterContentInit {

    @ContentChild(CfPresentComponent) cfpresetcomp: CfPresentComponent<any>;
    @Input() hasLoading: boolean;
    @Input() hasNoData: boolean;
    private state: CfDisplayState;

    constructor() { }

    ngAfterContentInit(): void {
       this.subPresentLoading();
    }

    private setState(state: CfDisplayState): void {
        this.state = state;
    }

    get getState(): CfDisplayState {
        return this.state;
    }

    subPresentLoading(): void {
        this.cfpresetcomp.stateChange$.subscribe((item) => {
            if (item) {
                this.setState(CfDisplayState.loading);
            } else {
                if (this.cfpresetcomp.hasData()) {
                    
                    this.setState(CfDisplayState.showData);
                } else {
                    
                    this.setState(CfDisplayState.noData);
                }
            }
        });
    }

    private checkHasNoDataComp(): boolean {
        return;
    }

    private checkHasLoadingComp(): boolean {
        return;
    }

    private createNoDataComp(): void {

    }

    private createLoadingComp(): void {

    }
}
