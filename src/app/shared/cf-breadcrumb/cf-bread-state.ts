import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";

export class CfBreadState {
    name: string;
    private text: string;
    private url: string;

    parsedtext : string;
    parsedurl: string;
    paramlist: Array<string>;
    private paramsMap:Object = {};
    constructor(data:CfIbreadState){
        this.name = data.name;
        this.text = data.text || "";
        this.url = data.url || "";
        this.parsedurl = data.url || "";
        this.parsedtext = data.text || "";
        this.setParamsList();

        
    }
    mapTextToParams(stateParams:Object){
        this.paramsMap = stateParams;
        this.parsedParams();
    }

    mapTextToParam(paramname,value){
        this.paramsMap[paramname] = value;
        this.parsedParams();
    }

    parsedParams(){
        this.parsedtext = this.text;
        this.parsedurl = this.url;
        for(const paramName of Object.keys(this.paramsMap)){
            this.parseTextToParam(paramName,this.paramsMap[paramName]);
        }
        
    }
    parseTextToParam(bindingname:string,value:string):void{
        
        //check there's bindingname in the params or not
        if(!this.paramlist.includes(bindingname)){
            throw new Error(`There's no "${bindingname}" binding in the text or url`);
        }
        bindingname = bindingname.replace(/\s*/g,'');
        const regexFindBindingName = new RegExp('{{\\s*'+ bindingname +'+\\s*}}','g');
        this.parsedtext = this.parsedtext.replace(regexFindBindingName,value);
        this.parsedurl = this.parsedurl.replace(regexFindBindingName,value);

    }

    private setParamsList():void{
        let params = [];
        const findParamsRegex = /{{\s*\w+\s*}}/g;
        params = [...params,...(this.text.match(findParamsRegex) || [])];
        params = [...params,...(this.url.match(findParamsRegex) || [])];
        this.paramlist = params.map((binding)=>{
            return binding.replace(/\s|{{|}}/g,'');
        });
    }


}