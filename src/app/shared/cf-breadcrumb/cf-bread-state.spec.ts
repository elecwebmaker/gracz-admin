
import { CfBreadState } from "app/shared/cf-breadcrumb/cf-bread-state";

describe('CfBreadState', function () {
    var cfBreadSate: CfBreadState;
    beforeEach(()=>{
        cfBreadSate = new CfBreadState({
            name:'product',
            text:'Hi, my name is {{name}}.',
            url:'I would love to {{ do }}{{something}}'
        });

    });

    it('should return correct parsed text and url with paramsMap',function(){
        cfBreadSate.mapTextToParam('name','pat');
        cfBreadSate.mapTextToParam('do','play');
        cfBreadSate.mapTextToParam('something','cat');
        expect(cfBreadSate.parsedtext).toEqual('Hi, my name is pat.');
        expect(cfBreadSate.parsedurl).toEqual('I would love to playcat');
    });

    it('should be able to change param value',function(){
        cfBreadSate.mapTextToParam('name','pat');
        cfBreadSate.mapTextToParam('name','patchange');
        expect(cfBreadSate.parsedtext).toEqual('Hi, my name is patchange.');
    });

    it('should throw an Error if there is no specify binding name in url or text',function(){
       

         expect(function(){ 
              cfBreadSate.mapTextToParam('none','pat');
        }).toThrow(new Error(`There's no "none" binding in the text or url`));
    });

    it('shoud has correct paramslist',function(){
        expect(cfBreadSate.paramlist.sort()).toEqual(['name','do','something'].sort());
    });
});