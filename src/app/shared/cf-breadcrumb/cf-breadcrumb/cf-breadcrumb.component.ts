import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { CfBreadState } from "app/shared/cf-breadcrumb/cf-bread-state";
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";
import { CfBreadCrumbHandler } from "app/shared/cf-breadcrumb/cf-breadcrumb-handler";

@Component({
  selector: 'app-cf-breadcrumb',
  templateUrl: './cf-breadcrumb.component.html',
  styleUrls: ['./cf-breadcrumb.component.scss']
})
export class CfBreadcrumbComponent implements OnInit,OnChanges {
  @Input() states: Array<CfIbreadState>;
  @Input() current: string;
  @Input() mapParams: Object;

  breadcrumbhandler:CfBreadCrumbHandler;
  constructor() { 

  }

  ngOnInit() {
    this.breadcrumbhandler = new CfBreadCrumbHandler(this.current,this.states,this.mapParams);
  }

  ngOnChanges(changes:SimpleChanges){
    if(this.breadcrumbhandler && changes){
      this.breadcrumbhandler.setParamsOfStates(this.mapParams);
    }
  }
}
