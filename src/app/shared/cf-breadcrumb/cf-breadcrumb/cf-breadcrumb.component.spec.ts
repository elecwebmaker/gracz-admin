import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CfBreadcrumbComponent } from './cf-breadcrumb.component';

describe('CfBreadcrumbComponent', () => {
  let component: CfBreadcrumbComponent;
  let fixture: ComponentFixture<CfBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CfBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CfBreadcrumbComponent);
    component = fixture.componentInstance;
    component.states = [
        {
            url:'testurl',
            name:'product',
            text:'Hi my name is {{name}}'
        },
        {
            url:'testurl2',
            name:'product2',
            text:'Hi my name is {{name}}2'
        },
        {
            url:'testurl2 of {{url}}',
            name:'product3',
            text:'Hi my name is {{name}}2'
        }
    ];
    component.current = 'product3';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should return correct breadlist',()=>{
    const expectedState =[
        {
            url:'testurl',
            name:'product',
            text:'Hi my name is '
        },
        {
            url:'testurl2',
            name:'product2',
            text:'Hi my name is 2'
        },
        {
            url:'testurl2 of ',
            name:'product3',
            text:'Hi my name is 2'
        }
    ];
    expect(component.breadcrumbhandler.breadlist.sort()).toEqual(expectedState);
  });
});
