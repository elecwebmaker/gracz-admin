import { CfBreadCrumbHandler } from "app/shared/cf-breadcrumb/cf-breadcrumb-handler";
import { CfBreadState } from "app/shared/cf-breadcrumb/cf-bread-state";


describe('CfBreadCrumbHandler',function(){
    var CfBreadCrumb:CfBreadCrumbHandler;
    beforeEach(()=>{
        var state = [
            {
                url:'testurl',
                name:'product',
                text:'Hi my name is {{name}}'
            },
            {
                url:'testurl2',
                name:'product2',
                text:'Hi my name is {{name}}2'
            },
            {
                url:'testurl2 of {{url}}',
                name:'product3',
                text:'Hi my name is {{name}}2'
            }
        ]
        CfBreadCrumb = new CfBreadCrumbHandler('product2',state,{
            product:{
                name:'pat'
            },
            product2:{
                name:'phung'
            }
        });
    });

    it('shoud return url with mapped parans',function(){
        const productState = CfBreadCrumb.getState('product');
        CfBreadCrumb.setMapParams();
        expect(productState.parsedtext).toEqual('Hi my name is pat');
    });
    
    it('shoud return mapParams same as provided in constructor',function(){
        const expectedMapParams = {
            product:{
                name:'pat'
            },
            product2:{
                name:'phung'
            }
        };
        expect(CfBreadCrumb.mapParamsWithState).toEqual(expectedMapParams);
    });


     it('shoud return mapParams object with specified key',function(){

        expect(Object.keys(CfBreadCrumb.mapParamsWithState)).toEqual(['product','product2']);
    });

    it('should throw an error if there is no state provided in mapParams',function(){
        const invalidParams = {
            testurl3:{
                name:'4'
            }
        };
        expect(function(){ CfBreadCrumb.setParamsOfStates(invalidParams)}).toThrow(new Error(`There's no testurl3 state.`));
        expect(function(){ CfBreadCrumb.setParamsOfState('none',{})}).toThrow(new Error(`There's no none state.`));

    });

    it('should can modify params of spcify state',function(){
        const params = {
            name:'patchange'
        };
        CfBreadCrumb.setParamsOfState('product',params)
        const productState = CfBreadCrumb.getState('product');
        expect(productState.parsedtext).toEqual('Hi my name is patchange');
    });

    it('should can modify params of states',function(){
        const changeMapParams = {
            product:{
                name:'patchange'
            },
            product2:{
                name:'phungchange'
            }
        };

        CfBreadCrumb.setParamsOfStates(changeMapParams);
        const productState = CfBreadCrumb.getState('product');
        const productState2 = CfBreadCrumb.getState('product2');
        expect(productState.parsedtext).toEqual('Hi my name is patchange');
        expect(productState2.parsedtext).toEqual('Hi my name is phungchange2');
    });

    it('should return correct breadlist',function(){
        const expectedBreadCrumb = [
            {
                url:'testurl',
                name:'product',
                text:'Hi my name is pat'
            }
        ];
        const expectedBreadCrumb2 = [
            {
                url:'testurl',
                name:'product',
                text:'Hi my name is pat'
            },
            {
                url:'testurl2',
                name:'product2',
                text:'Hi my name is phung2'
            }
        ];
        const expectedBreadCrumb3 = [
            {
                url:'testurl',
                name:'product',
                text:'Hi my name is pat'
            },
            {
                url:'testurl2',
                name:'product2',
                text:'Hi my name is phung2'
            },
            {
                url:'testurl2 of ',
                name:'product3',
                text:'Hi my name is 2'
            }
        ];
        CfBreadCrumb.current = 'product';        
        expect(CfBreadCrumb.breadlist.sort()).toEqual(expectedBreadCrumb.sort());
        CfBreadCrumb.current = 'product2';
        expect(CfBreadCrumb.breadlist.sort()).toEqual(expectedBreadCrumb2.sort());
        CfBreadCrumb.current = 'product3';
        expect(CfBreadCrumb.breadlist.sort()).toEqual(expectedBreadCrumb3.sort());
    });


    it('should throw an error if there is no state provided in current',function(){
        expect(function(){
            CfBreadCrumb.current = 'none'; 
        }).toThrow( new Error(`There's no none state that you provide in current`))
        
    });
});