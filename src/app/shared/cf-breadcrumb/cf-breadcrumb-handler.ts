
import { CfIbreadState } from "app/shared/cf-breadcrumb/cf-ibread-state";
import { CfBreadState } from "app/shared/cf-breadcrumb/cf-bread-state";

export class CfBreadCrumbHandler {
    get current(): string{
        return this._current;
    }
    set current(val){
        if(!this.checkThereIsState(val)){
            throw new Error(`There's no ${val} state that you provide in current`);
        }
        
        this._current = val;
        this.parseBreadlist();
    }
    _current: string;
    mapParamsWithState: Object = {};
    states: Array<CfBreadState>;
    breadlist: Array<{
        url: string;
        text: string;
        name: string;
    }> = [];

    constructor(current: string, states: Array<CfIbreadState>, mapParams = {}) {
        
        this.states = states.map((state) => {
            return new CfBreadState(state);
        });
        this.setParamsOfStates(mapParams);
        this.current = current;
    }

    setParamsOfStates(paramsWithState: Object) {
        for(const statename of Object.keys(paramsWithState)){
            if(!this.checkThereIsState(statename)){
                throw new Error(`There's no ${statename} state.`);
            }
        }
        this.mapParamsWithState = paramsWithState;
        this.setMapParams();
    }

    setParamsOfState(statename,params){
        if(this.checkThereIsState(statename)){
             this.mapParamsWithState[statename] = params;
        }else{
            throw new Error(`There's no ${statename} state.`);
        }
        this.setMapParams();
       
    }

    setMapParams() {
        for (let stateName of Object.keys(this.mapParamsWithState)) {
        
            const stateObj = this.getState(stateName);
            const stateParams = this.mapParamsWithState[stateName];
            stateObj.mapTextToParams(stateParams);
        };
        this.parseBreadlist();
    }

    getState(statename) {
        const stateObj = this.states.filter((state) => {
            return state.name == statename;
        })[0];
        if (stateObj) {
            return stateObj;
        }
    }

    checkThereIsState(statename){
        const stateObj = this.states.filter((state) => {
            return state.name == statename;
        })[0];
        if (stateObj) {
            return true;
        } else {
            return false;
        }
    }

    parseBreadlist(){
        this.breadlist = [];
        for(const state of this.states){
      
            this.breadlist.push({
                name:state.name,
                url:this.parseUnResolvedParam(state.parsedurl),
                text:this.parseUnResolvedParam(state.parsedtext)
            });
            if(state.name == this.current){
                break;
            }
        };
    }

    parseUnResolvedParam(message:string= ""):string{
        const findParamsRegex = /{{\s*\w+\s*}}/g;
        return message.replace(findParamsRegex,'');
    }
}